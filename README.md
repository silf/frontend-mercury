# Mercury #

### Build status ###

[![master status](https://semaphoreci.com/api/v1/ilf/frontend-mercury/branches/master/badge.svg)](https://semaphoreci.com/ilf/frontend-mercury)

## Development ##


### Setup development environment ###

#### With Docker ####

1. init project

        #!bash
        $ ./dev.sh init
    
2. check configuration for development environment in files:
    * `config/dev.json`
    * `config/localServerConfig.coffee` - for sure change domain and url, and check rest of config params
    
3. run development server:

        #!bash
        $ ./dev.sh start

#### Without Docker ####

1. install node.js and npm

2. install npm packages:

        #!bash
        $ sudo npm install -g grunt-cli
        $ sudo npm install -g bower

3. clone project

4. resolve dependencies

	* js packages (inside project dir):

            #!bash
            $ npm install
            $ bower install

	* submodules:

            #!bash
            $ git submodule init
            $ git submodule update

        or download [jsjac](https://github.com/sstrigler/JSJaC) into `app/libs/jsjac`

	* inside `app/libs/jsjac`: `$ make`

5. check configuration for development environment in files:
    * `config/dev.json`
    * `config/localServerConfig.coffee` - for sure change domain and url, and check rest of config params

6. run development server:

        #!bash
        $ grunt server


### Code guidelines ###

* [CoffeeScript in AngularJS app](http://alxhill.com/blog/articles/angular-coffeescript/)
* [AngularJS: service vs. factory](http://iffycan.blogspot.co.uk/2013/05/angular-service-or-factory.html)
* [Development conventions](https://www.npmjs.org/package/ng-classify)
* When you want to add new translations in *.coffee files wrap text in '@gettext' function. Remember to inject it to your controller. Then follow below steps to generate new versions of *.pot, *.po and translations.js files:
    1. # update js files in dist and tmp
       $ grunt build
    2. # update *.pot file with data from *.js files
       $ grunt nggettext_extract
    3. open *.po files in your favorite pofile editor (like poedit) and update *.po files with new version of *.pot. Add missing translations.
    4. # update translations.js from new version of *.po files
       $ grunt build


### Contribution ###

The [gitflow workflow](http://nvie.com/posts/a-successful-git-branching-model) is used in this project,
so allowed branch names are:

* master
* develop
* feature/<feature_name>
* release/<version>
* bugfix<bug_id>
* hotfix/<bug_id>

We strongly recommend using [git-flow extension](https://github.com/nvie/gitflow)
with very useful set of [commands](http://danielkummer.github.io/git-flow-cheatsheet).

