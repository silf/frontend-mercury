/**
 * Add endsWith for this browsers which do not support it yet.
 * solution taken from 
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith
 */
if (!String.prototype.endsWith) {
  Object.defineProperty(String.prototype, 'endsWith', {
    value: function(searchString, position) {
      var subjectString = this.toString();
      if (position === undefined || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.indexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
    }
  });
}

/**
 * Add patch for JSJaCWebSocketConnection.prototype._parseXml.
 * Problem is with wrapping stanza into stream tag. Stream errors contains
 * also tag for closing stream = </stream> which leads to wrong xml syntax.
 * This patch adds if statement to check if closing stream is part of incomming
 * stanza. If yest only starting tag is added.
 */
JSJaCWebSocketConnection.prototype._parseXml = function(s) {
    var doc;

    this.oDbg.log('Parsing: ' + s, 4);
    try {
        doc = XmlDocument.create('stream', NS_STREAM);
        if(s.indexOf('<stream:stream') === -1) {
            // Wrap every stanza into stream element, so that XML namespaces work properly.
            if (s.trim().endsWith("</stream:stream>"))
                doc.loadXML("<stream:stream xmlns:stream='" + NS_STREAM + "' xmlns='jabber:client'>" + s);
            else
                doc.loadXML("<stream:stream xmlns:stream='" + NS_STREAM + "' xmlns='jabber:client'>" + s + "</stream:stream>");
            return doc.documentElement.firstChild;
        } else {
            doc.loadXML(s);
            return doc.documentElement;
        }
    } catch (e) {
        this.oDbg.log('Error: ' + e);
        this._connected = false;
        this._handleEvent('onerror', JSJaCError('500', 'wait', 'internal-service-error'));
    }

    return null;
};

/**
 * Add patch for JSJaCWebSocketConnection.prototype._onclose.
 * Change generataed event from service-unavailable to service-closed
 * which is uniq one.
 */
JSJaCWebSocketConnection.prototype._onclose = function() {
    this.oDbg.log('websocket closed', 2);
    if (this._status !== 'disconnecting') {
        this._connected = false;
        this._handleEvent('onerror', JSJaCError('503', 'cancel', 'service-closed'));
    }
};

