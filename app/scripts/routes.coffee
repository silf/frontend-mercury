'use strict'

class Routes extends Config
    constructor: ($routeProvider, $locationProvider) ->
        $routeProvider
        .when '/',
            templateUrl: 'views/main.html'
            controller: 'mainCtrl'
        .when '/experiment/:name',
            templateUrl: 'views/experiment.html'
            controller: 'genericExperimentCtrl'
        .when '/console',
            templateUrl: 'views/console.html'
            controller: 'consoleCtrl'
        .when '/client',
            templateUrl: 'views/client.html'
            controller: 'clientCtrl'
        .when '/fake',
            templateUrl: 'views/fake.html'
            controller: 'fakeCtrl'
        .when '/help',
            templateUrl: 'views/help.html'
        .otherwise
            redirectTo: '/'
        $locationProvider.hashPrefix '!'
        $locationProvider.html5Mode yes
