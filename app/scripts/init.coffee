'use strict'

###*
* @ngdoc function
* @name mercury#run
* @methodOf mercury
* @description Initialization code
###
class Init extends Run
    constructor: ->
        $( ->
            $(document).on('click', '.dropdown-menu.dropdown-menu-form', (e) ->
                e.stopPropagation()
            )
            $.material.init()
        )
