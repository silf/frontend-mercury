'use strict'

###*
* @ngdoc service
* @id mercury.factory:experiment
* @name mercury.Experiment
* @requires mercury.service:xmpp
* @requires mercury.factory:stanza
* @description Experiment class
* which can be instantiated
###
class Experiment extends Factory

    constructor: (xmppService, Stanza, configService, languageService) ->

        return class ExperimentInstance

            constructor: (address) ->
                @address = address
                @callbacks = []
                @connected = false
                xmppService.onConnectionResumed(@reconnect)
                @protocolVersion = null
                @protocolVerSuccessCallback = null
                @protocolVerErrorCallback = null
                @disconnectCallback = null
                # no need to clear this list of callbacks. Experiment is created per instance of genericExperiment
                # so there is new object each time user is switching experiments
                @expStopCallbacks = []
                @versionUtil = new VersionUtil(configService.supportedProtocolVersions)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#connect
            * @param {string} alias experiment alias
            * @description connects to experiment
            *
            * Connection is realized by connecting
            * current user to xmpp groupchat dedicated
            * for chosen experiment.
            ###
            connect: (alias) =>
                @alias = alias
                @connected = xmppService.connectToGroupchat @address, @alias

                # register stanza handlers
                onProtocolSet = (ver) =>
                    # version was send as first thing - experiment is in progress
                    if @versionUtil.isProtocolVersionSupported([ver.version])?
                        @protocolVersion = ver.version
                    else
                        alert @UNSUPPORTED_PROTOCOL_VERSION_MSG

                @registerLabdataCallback 'silf:protocol:version:set', null, onProtocolSet, null, null, 'chat'

                onExperimentStop = () =>
                    for stopCallback in @expStopCallbacks
                        stopCallback()
                    @clearExperimentData()

                @registerLabdataCallback 'silf:experiment:stop', null, onExperimentStop

                return @connected

            clearExperimentData: =>
                @protocolVersion = null
                @protocolVerSuccessCallback = null
                @protocolVerErrorCallback = null
                #@disconnectCallback = null
                @expStopCallbacks = []

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#reconnect
            * @description reconnects with experiment when connection was lost
            *
            * When xmppp connection is lost with server and is resumed this is callback
            * which is executed after successful reconnection on xmpp level. This callback
            * checks if user was connected to experiment and if yes reEnters experiment room.
            * after that registered callback for resumed connection is executed.
            ###
            reconnect: () =>
                if not @connected
                    @connectionResumedCallback()
                    return
                @connected = xmppService.connectToGroupchat @address, @alias
                if @connected
                    @connectionResumedCallback()
                else
                    @connectionNotResumedCallback()

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#disconnect
            * @description disconnects from experiment
            *
            * Disconnection is realized by leaving
            * groupchat dedicated for chosen experiment
            * by current user. All callbacks are unregistered.
            ###
            disconnect: =>
                @unregisterAllCallbacks()
                xmppService.leaveGroupchat(@address, @alias)
                @connected = false
                if @disconnectCallback?
                    runOnceFun = @disconnectCallback
                    # run in timeout because disconnectCallback executes scope.apply and
                    # disconnect call can be done inside of angular
                    setTimeout(runOnceFun, 1)
                    @disconnectCallback = null

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#getModes
            * @param {function} callback function invoked after experiment reply. Callback has no arguments
            * @param {function} errorCallback function invoked if some error occurred during mode setting. One argument
            * is passed to function, this is error message.
            * @description Retrives list of avilable modes for experiment. Executed only by
            * experiment operator.
            ###
            getModes: (callback, errorCallback) =>
                # negotiate protocol version before asking for experiment modes
                # if protocol version is set then ask for modes
                onNegotiateProtocolVersion = () =>
                    onLanguageSet = () =>
                        @query 'silf:mode:get', null, callback
                    @setLanguage onLanguageSet

                @negotiateProtocolVersion(onNegotiateProtocolVersion, errorCallback)

            UNSUPPORTED_PROTOCOL_VERSION_MSG: "Nie można nawiązać połączenia z eksperymentem. " +
                "Nie wspierana wersja protokołu.\nOdśwież strone i spróbuj ponownie."

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#negotiateProtocolVersion
            * @param {function} callback function invoked after negotiation of protocol version is finished. Callback has no arguments
            * @param {function} errorCallback function invoked if some error occurred during protocol negotiation.
            * One argument is passed to function, this is error message.
            * @description Negotiate protocol version, if successful execute callback.
            * experiment operator.
            ###
            negotiateProtocolVersion: (callback, errorCallback) =>
                if @protocolVersion?
                    callback()
                    return
                onProtocolVersionGet = (protocolList) =>
                    selectedVersion = @versionUtil.isProtocolVersionSupported(protocolList.versions)
                    if selectedVersion != null
                        @setProtocolVersion(selectedVersion, callback, errorCallback)
                    else
                        console.error "Niewspierana wersja protokolu: "
                        console.error protocolList.versions
                        errorCallback(@UNSUPPORTED_PROTOCOL_VERSION_MSG)

                @getProtocolVersion(onProtocolVersionGet, callback)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#getProtocolVersion
            * @param {function} callback function invoked after list of supported versions is recived. Callback has one argument
            * this is list of supported versions
            * @param {function} verNotSupportedCallback function invoked if feature of protocol versioning is not supported. Callback has no arguments
            * @description Get supported protocol versions
            * experiment operator.
            ###
            getProtocolVersion: (callback, verNotSupportedCallback) =>
                # [ProtVer] below is timeout execution of MODE SET for those experiments not supporting protocol versions
                getProtocolVerTimer = setTimeout () ->
                    console.warn "Experiment does not support protocol versioning - skip it"
                    verNotSupportedCallback()
                , configService.protocolVersionTimeout

                onVersion = (protocolList) ->
                    # [ProtVer] delete below when all experiments will be migrated to protocol visioning
                    clearTimeout getProtocolVerTimer
                    callback(protocolList)
#                @query 'silf:protocol:version:get', null, callback
                @query 'silf:protocol:version:get', null, onVersion

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#setProtocolVersion
            * @param {function} callback function invoked after protocol version is set
            * @param {function} errorCallback function invoked if some error occurred during protocol negotiation.
            * One argument is passed to function, this is error message.
            * @description Set protocol version to one passed as first argument. Execute callback if successful or
            * errorCallback if some problems occurred
            * experiment operator.
            ###
            setProtocolVersion: (selectedVersion, callback, errorCallback) =>
                @protocolVersion = selectedVersion
                @protocolVerSuccessCallback = callback
                @protocolVerErrorCallback = errorCallback
                @query 'silf:protocol:version:set', {"version": selectedVersion}, @onProtocolSet, @onProtocolSetError

            ###
                Callback executed when setting protocol version is successful
            ###
            onProtocolSet: (ver) =>
                if not @protocolVersion?
                    # it is either observer already present in room or returning operator
                    @protocolVersion = ver.version
                else if (@protocolVersion == ver.version)
                    @protocolVerSuccessCallback()
                else
                    console.error "Niezgodna wersja protokolu:" + ver.version
                    @protocolVerErrorCallback(@UNSUPPORTED_PROTOCOL_VERSION_MSG)

            ###
                Callback executed when setting protocol version has errors
            ###
            onProtocolSetError: (errData) =>
                em = "Nie mozna przeprowadzic eksperyment - wystąpił błąd połączenia.\n" +
                    "Odśwież strone i spróbuj ponownie."
                if (errData and errData[0].metadata.message == "No supported protocol version")
                    em = @UNSUPPORTED_PROTOCOL_VERSION_MSG
                @protocolVerErrorCallback(em)


            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#setLanguage
            * @param {function} callback function invoked after experiment reply after language is set,
            *        or immediately if experiment doesn't support languages
            * @description sets current language
            ###
            setLanguage: (callback) =>
                lang = languageService.currentLanguage.substring(0, 2)
                errorCallback = (errors) ->
                    console.error "cannot set language #{lang}, the error is:"
                    console.error errors
                    callback()
                langData =
                    langs: [lang]
                if lang and @protocolVersion and @versionUtil.versionIsAtLeast(@protocolVersion, '1.1.0')
                    @query 'silf:lang:set', langData, callback, callback
                else
                    callback()

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#setModes
            * @param {string} mode chosen mode
            * @param {function} callback function invoked after experiment reply
            * @description sets chosen mode
            ###
#            setMode: (mode, callback) =>
            setMode: (mode) =>
#                @query 'silf:mode:set', { mode: mode }, callback
                @query 'silf:mode:set', { mode: mode }

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#startSeries
            * @param {object} settings experiment settings
            * @param {function} callback function invoked when series start is successful
            * @param {function} errorCallback function invoked when start is not possible due to errors in settings
            * @param {object} callbackData data passed to callbacks
            * @description starts experiment series with provided settings
            ###
            startSeries: (settings, callback, errorCallback, callbackData) =>
                @query 'silf:series:start', settings, callback, errorCallback, callbackData

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#stopSeries
            * @param {string} seriesId series identifier
            * @description stops experiment series with provided identifier
            ###
            stopSeries: (seriesId) =>
                @query 'silf:series:stop', { seriesId: seriesId }

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#checkSettings
            * @param {object} settings experiment settings to validate
            * @param {function} callback function invoked after validation success
            * @param {function} errorCallback function invoked after validation error
            * @param {object} callbackData data passed to callbacks
            * @description validates provided experiment settings
            ###
            checkSettings: (settings, callback, errorCallback, callbackData) =>
                @query 'silf:settings:check', settings, callback, errorCallback, callbackData

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#updateSettings
            * @param {object} settings experiment settings to update
            * @param {function} callback function invoked after validation success
            * @param {function} errorCallback function invoked after validation error
            * @param {object} callbackData data passed to callbacks
            * @description updates provided experiment settings
            ###
            updateSettings: (settings, callback, errorCallback, callbackData) =>
                @query 'silf:settings:update', settings, callback, errorCallback, callbackData

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#stop
            * @description stops experiment
            ###
            stop: =>
                @query 'silf:experiment:stop'

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#getState
            * @param {function} callback function invoked after experiment reply
            * @description checks current status of experiment
            ###
            getState: (callback) =>
                @query 'silf:state:get', null, callback

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onSetMode
            * @param {function} callback function invoked when event occures
            * @description registers event handler for set mode stanza
            ###
            onSetMode: (callback) =>
                @registerLabdataCallback('silf:mode:set', null, callback)
                @registerLabdataCallback 'silf:mode:set', null, callback, null, null, 'chat'

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onResult
            * @param {function} callback function invoked when event occures
            * @description registers event handler for experiment results
            ###
            onResult: (callback) =>
                console.debug 'registering callback for results'
                @registerLabdataCallback('silf:results', null, callback)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onStartSeries
            * @param {function} callback function invoked when event occures
            * @description registers event handler for start experiment series
            ###
            onStartSeries: (callback, errorCallback = null, callbackData = null) =>
                @registerLabdataCallback('silf:series:start', null, callback, errorCallback, callbackData)
                # observer gets private messages with type 'chat' and should react only on success stanzas
                @registerLabdataCallback 'silf:series:start', null, callback, null, null, 'chat'

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onStopSeries
            * @param {function} callback function invoked when event occures
            * @description registers event handler for stop experiment series
            ###
            onStopSeries: (callback) =>
                @registerLabdataCallback('silf:series:stop', null, callback)

            onExperimentStop: (callback) =>
                @expStopCallbacks.push(callback)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onDisconnect
            * @param {function} callback function invoked when event occurs
            * @description registers event handler for disconnect from experiment
            *
            * `ondisconnect` event is fired by {@link mercury.service:xmpp xmpp client} when user is disconnected
            * from xmpp server. Connection is ended with xmpp server.
            ###
            onDisconnect: (callback) =>
                @disconnectCallback = callback
                @on('ondisconnect', callback)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onConnectionLost
            * @param {function} callback function invoked when event occurs
            * @description registers event handler for unexpected loosing of connection
            *
            * `onConnectionLost` event is fired by {@link mercury.service:xmpp xmpp client} when xmpp connection
            * to the server is lost.
            ###
            onConnectionLost: (callback) ->
                xmppService.onConnectionLost(callback)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onConnectionResumed
            * @param {function} callback function invoked when event occurs
            * @description registers event handler for successful connection resumed after it has been lost
            *
            * `onConnectionResumed` event is fired by {@link mercury.service:xmpp xmpp client} when xmpp connection
            * is resumed after it has been lost due to onConnectionLost event.
            ###
            onConnectionResumed: (callback) =>
                @connectionResumedCallback = callback

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onConnectionNotResumed
            * @param {function} callback function invoked when event occurs
            * @description registers event handler for failure with connection resumtion.
            *
            * `onConnectionNotResumed` event is fired by {@link mercury.service:xmpp xmpp client} when xmpp connection
            * was not resumed after few try's.
            ###
            onConnectionNotResumed: (callback) =>
                @connectionNotResumedCallback = callback
                xmppService.onConnectionNotResumed(callback)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onError
            * @param {function} callback function invoked when event occures
            * @description registers event handler for experiment errors
            ###
            onError: (callback) =>
                handler = (packet) ->
                    console.debug 'error'
                    result = packet.getLabData()
                    callback(result.data.errors)
                @on(['message', 'labdata', 'silf:misc:error', 'groupchat'], handler)


            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onBecomeOperator
            * @param {function} callback function invoked when event occures
            * @description registers event handler for becoming experiment operator
            ###
            onBecomeOperator: (callback) =>
                handler = (packet) =>
                    console.debug 'presence'
                    item = packet.getChild('item')
                    nick = item.getAttribute('nick')
                    role = item.getAttribute('role')
                    # check if participant is given in experiment control room
                    if @isPacketFromControllRoom(packet) and nick == @alias and role in ['participant', 'moderator']
                        callback()
                @on(['presence', 'item'], handler)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#onBecomeObserver
            * @param {function} callback function invoked when event occures
            * @description registers event handler for becoming experiment observer
            ###
            onBecomeObserver: (callback) =>
                handler = (packet) =>
                    console.debug 'presence'
                    item = packet.getChild('item')
                    nick = item.getAttribute('nick')
                    role = item.getAttribute('role')
                    if @isPacketFromControllRoom(packet) and nick == @alias and role == 'visitor'
                        callback()
                @on(['presence', 'item'], handler)


            ################ HELPER METHODS ##############

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#isPacketFromControllRoom
            * @param {object} packet received from xmpp connection
            * @description return true if packet is from experiment control room
            ###
            isPacketFromControllRoom: (packet) =>
                packet.getFrom().indexOf(@address) == 0

            on: (event, callback) =>
                @callbacks.push {
                    event: if event instanceof Array then event[0] else event
                    callback: callback
                }
                xmppService.on(event, callback)
                console.debug "cb registered"
                console.debug @callbacks

            off: (event, callback) =>
                console.debug "cb unregistering"
                rm = {
                    event: event
                    callback: callback
                }
                if (t = @callbacks.indexOf(rm)) > -1
                    @callbacks[t..t] = []
                    console.debug "cb found on position #{t}"
                else
                    console.debug "cb not found"
                xmppService.off(event, callback)
                console.debug @callbacks

            unregisterAllCallbacks: =>
                for rm in @callbacks
                    xmppService.off(rm.event, rm.callback)
                @callbacks = []

            query: (xmlns, data, callback = null, errorCallback = null, callbackData = null) =>
                labdata = new Stanza.LabData(
                    type: 'query'
                    xmlns: xmlns
                    data: data
                )
                callbackDataLocal = callbackData
                @registerLabdataCallback(xmlns, labdata.id, callback, errorCallback, callbackData)
                xmppService.sendLabMsg(labdata, @address, yes)

            ###*
            * @doc method
            * @methodOf mercury.factory:experiment
            * @name Experiment#registerLabdataCallback
            * @param {string} xmlns name space of labdata stanza for which callbacks are registered
            * @param {string} id optional parameter. If null then callback is registered for given namespace.
            * If not null then callback is registered for stanza with given id, and unregistered after stanza is received.
            * @param {function} callback function invoked after result of type done or result is received
            * @param {function} errorCallback function invoked after result of type error is received
            * @param {object} callbackData data passed to callbacks
            * @param {string} messageType type of message, can be 'groupchat' for messages to all occupants of room
                                            or 'chat' if message is send between two persons in room an is only between them
            * @description registers callback for labdata stanza with given namespace and optionally id.
            ###
            registerLabdataCallback: (xmlns, id = null, callback = null, errorCallback = null, callbackData = null, messageType = 'groupchat') =>
                callbackDataLocal = callbackData
                runForId = id

                if callback or errorCallback
                    handler = (packet) =>
                        console.debug packet
                        result = packet.getLabData()
                        if not result? or (runForId != null and runForId != result.id)
                            console.debug 'nie mój'
                            return

                        console.debug 'mój ' + xmlns
                        if callback and (result.type == 'done' or result.type == 'result' or result.type == 'data')
                            if runForId == result.id
                                @off('message', handler)
                            callback(result.data, callbackDataLocal)
                        else if errorCallback and result.type == 'error'
                            if runForId == result.id
                                @off('message', handler)
                            errorCallback(result.data.errors, callbackDataLocal)
                        else
                            console.debug 'brak obslugi: ' + xmlns + " type: " + result.type
                    @on(['message', 'labdata', xmlns, messageType], handler)

    # util class for operations on protocol version
    class VersionUtil

        constructor: (@supportedVersions) ->

        isProtocolVersionSupported: (serverVersions) =>
            for accVer in @supportedVersions
                if @versionPresentInList(accVer, serverVersions)
                    return accVer
            return null

        ###
        * @param ver
        * @param listOfVersions
        ###
        versionPresentInList: (ver, listOfVersions) =>
            for accVer in listOfVersions
                if accVer.indexOf('-') != -1
                    accVer = accVer.split("-")
                    if accVer[0] == ver || (@versionIsLower(accVer[0], ver) && @versionIsLower(ver, accVer[1]))
                        return true
                else if ver == accVer
                    return true

            return false

        versionIsLower: (left, right) ->
            l_tab = [parseInt(subV) for subV in left.split(".")]
            r_tab = [parseInt(subV) for subV in right.split(".")]
            return l_tab[0] < r_tab[0] or
                (l_tab[0] == r_tab[0] and l_tab[1] < r_tab[1]) or
                (l_tab[0] == r_tab[0] and l_tab[1] == r_tab[1] and l_tab[2] < r_tab[2])

        versionIsAtLeast: (version, minVersion) =>
            not @versionIsLower(version, minVersion)
