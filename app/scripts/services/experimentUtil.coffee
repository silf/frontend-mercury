'use strict'
###*
* @ngdoc service
* @id mercury.service:experimentUtil
* @name mercury.experimentUtilService
* @requires mercury.service:language
* @description Encapsulates common code for experiment operations
###
class ExperimentUtil extends Service

    constructor: (@languageService) ->
        console.debug('ExperimentUtil creation')

    ###*
    * displayNameDict can contain either simple string object or string representing json dictionary, where
    * language code is the key.
    * We treat 'en' as default language. If current language is missing in the dictionary than value for en is
    * returned. If it is also missing than defaultDisplayName is used.
    ###
    getExperimentDisplayName: (displayNameDict, defaultDisplayName) =>
        displayNameDict = displayNameDict?.trim()
        if displayNameDict?.indexOf('{') == 0
            try
                translations = JSON.parse(displayNameDict)
                displayVal = translations[@languageService.currentLangCode()]
                if not displayVal?
                    displayVal = translations['en']
                return displayVal ? defaultDisplayName
            catch err
                console.error('Error while parsing experiment display name')
                console.error(err)
                return defaultDisplayName

        return displayNameDict
