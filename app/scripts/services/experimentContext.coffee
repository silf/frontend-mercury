'use strict'


###*
* @ngdoc service
* @id mercury.service:experimentContext
* @name mercury.experimentContextService
* @requires mercury.service:config
* @description Context of the running experiment
###
class ExperimentContext extends Service

    constructor: (configService) ->
        return new ExperimentContextHolder(configService)

class ExperimentContextHolder

    constructor: (@configService) ->
        @currentExperiment = null
        @experimentSwitchListeners = []
        @generalChatMsessages = []

    ###
     Add listener for event of switching the experiment by user.
     Listener will be executed before location for new experiment is set
    ###
    addSwitchListener: (callback) =>
        if (@experimentSwitchListeners.indexOf(callback) < 0)
            @experimentSwitchListeners.push callback

    ###
     Remove listener for event of switching the experiment by user.
    ###
    removeSwitchListener: (callback) =>
        idx = @experimentSwitchListeners.indexOf(callback)
        if (idx >= 0)
            @experimentSwitchListeners = @experimentSwitchListeners.slice(idx, 1)

    ###
     Execute listeners for event of switching the experiment
    ###
    onExperimentSwitch: (newExperiment) =>
        console.debug "onExperimentSwitch " + @experimentSwitchListeners.length
        for l in @experimentSwitchListeners
            try
                l(@currentExperiment, newExperiment)
            catch err
                console.error err
        @currentExperiment = newExperiment

    storeGeneralChatMessages: (msgs) =>
        @generalChatMsessages = msgs

    restoreGeneralChatMessages: () =>
        return @generalChatMsessages
