'use strict'

###*
* @ngdoc service
* @id mercury.factory:utils
* @name mercury.Utils
* @requires ng.$sce
* @description various useful functions
###
class Utils extends Factory

    constructor: ($sce, gettext) ->
        return new UtilsInstance($sce, gettext)


class UtilsInstance
    constructor: (@$sce, @gettext) ->

    uuid: ->
        'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) ->
            r = Math.random() * 16 | 0
            v = if c is 'x' then r else (r & 0x3 | 0x8)
            v.toString(16)
        )

    #TODO korzystanie z tej mamy nie DZIALA!! czemu?
#        uiGenerators: {
#            boolean: @defaultControlGenerator,
#            interval: @defaultControlGenerator,
#            number: @defaultControlGenerator,
#            range: @rangeControlGenerator
#        }

    onChangeClass: 'checkSett'

    generateControlForSettings: (sett, readonly = false) ->

        fabricMap =
            range: @rangeControlGenerator
            number: @numberControlGenerator
            interval: @numberControlGenerator
            boolean: @booleanControlGenerator
            button: @buttonControlGenerator
            'combo-box': @comboboxControlGenerator

        for k, desc of sett
            console.info desc
            fabricate = @defaultControlGenerator
            if desc.type of fabricMap
                console.warn "#{k} : #{desc.type}"
                fabricate = fabricMap[desc.type]

            desc.control = fabricate(k, desc)

            if @typeIsArray desc.control
                desc.decorate = desc.control[2]
                desc.wrapper = desc.control[1]
                desc.control = desc.control[0]
            else
                desc.decorate = true
                desc.wrapper = desc.control

            # sett common part for each controll
            desc.control.attr('id', k)
            if readonly
                desc.control.prop('disabled', true)
            desc.control.addClass @onChangeClass
            if desc.decorate
                desc.control.addClass 'form-control'

            desc.wrapper = @$sce.trustAsHtml desc.wrapper.prop 'outerHTML'
            # add execute flag which is used to mark controls with which user interacted
            # and validation should be executed
            if not desc.validations?
                desc.validations = {}
            desc.validations.execute = false

    ###
    Enable settings panel so that operator can control equipment.
    ###
    enableSettings: (settingsMap, experimentObj, formScopeObj) ->
        $('#settingsForm .form-control').prop('disabled', false)
        @registerOnChangeEventForSettingsForm(settingsMap, experimentObj, formScopeObj)

    disableNonLiveSettings: (settings) =>
        for ctl in @getNonLiveControls(settings)
            ctl.prop('disabled', true)

    enableNonLiveSettings: (settings) =>
        for ctl in @getNonLiveControls(settings)
            ctl.prop('disabled', false)

    getNonLiveControls: (settings) =>
        controls = []
        for k, desc of settings
            if not desc.live
                controls.push $('#settingsForm').find("##{desc.control[0].id}")
        controls

    registerOnChangeEventForSettingsForm: (settingsMap, experimentObj, formScopeObj) ->
        # add onChange handler for settings controls
        that = this
        evFun = (eventObj) ->
            console.debug 'Handler for .change() called.'
            _settingsMap = eventObj.data[0]
            changedSetting = eventObj.target.id
            _settingsMap[changedSetting].validations.execute = true

            # collect data from the form
            settingsData = that.getSeriesData(_settingsMap)

            # remove data which was not yet touched  by user
            for key, desc of _settingsMap
                if not _settingsMap[key].validations.execute
                    delete settingsData[key]

            # add current property
            for key, desc of settingsData
                desc.current = (key == changedSetting)
                if _settingsMap[key].type == 'button'
                    desc.value = desc.current

            console.warn settingsData

            experiment = eventObj.data[1]
            callbackData =
                experiment: experiment
                settingsMap: settingsMap
                scope: formScopeObj
            experiment.checkSettings settingsData, that.onSettingsCheckWithoutErrors, that.onSettingsCheckWithErrors, callbackData
        evObj = $( ".#{@onChangeClass}" ).change([settingsMap, experimentObj], evFun)
        $( "button.#{@onChangeClass}" ).click([settingsMap, experimentObj], evFun)

        return true

    unregisterOnChangeEventForSettingsForm: () ->
        $( ".#{@onChangeClass}" ).off("change")
        $( "button.#{@onChangeClass}" ).off("click")
        return true

    # called when after settings check there are no errors
    # callbackData is the scope object from which registration took place
    onSettingsCheckWithoutErrors: (result, callbackData) =>
        $("#settingsForm span.help-block").each(() ->
            $(this).hide()
        )
        $("#settingsForm .form-group").each(() ->
            $(this).removeClass("has-error")
        )

        callbackData.scope.$apply( (scope) ->
            scope.settingsForm.$setValidity('remote-error', true)
        )

        @updateLiveSettings result, callbackData.settingsMap, callbackData.experiment

        return true

    updateLiveSettings: (controls, settingsMap, experiment) ->
        liveControls = new -> @[key] = value for key, value of controls when settingsMap[key].live; @
        experiment.updateSettings liveControls

    #TODO create separate module for all velidation code
    # callback when validation errors are recieved from xmmp server
    # callbackData is the scope object from which registration took place
    onSettingsCheckWithErrors: (errors, callbackData) ->
        console.log 'onSettingsCheckWithErrors'
        # clear errors from fields which are now containing errors any more
        errorFields = [e.metadata.field_name for e in errors]
        $("#settingsForm .form-group.has-error").each(() ->
            $th = $(this)
            fieldName = $th.attr('id')
            fieldName = fieldName.substring(0, fieldName.length - 'Container'.length)
            if errorFields.indexOf(fieldName) == -1
                $th.removeClass("has-error")
                hb = $th.find("span.help-block")
                if hb.length?
                    hb.hide()
        )
        if errors.length > 0
            callbackData.scope.$apply( (scope) ->
                scope.settingsForm.$setValidity('remote-error', false)
            )

        # display errors
        for e in errors
            $("#settingsForm #" + e.metadata.field_name + "Container").addClass("has-error")
            errElem = $("#settingsForm #" + e.metadata.field_name + "Container span.help-block")
            if errElem.length?
                errElem.text(e.metadata.message)
                errElem.show()

    defaultControlGenerator: (name, desc) ->
        # console.debug 'defaultControlGenerator 4 ' + name
        newElem = angular.element("<input />")
        newElem.attr("type", "text")
        newElem.attr("placeholder", @gettext('enter value'))
        newElem.attr 'value', desc.default_value
        console.debug newElem
        newElem

    booleanControlGenerator: (name, desc) ->
        newElem = angular.element("<input />")
        newElem.attr("type", "checkbox")
        newElem.attr 'value', desc.default_value
        wrapper = angular.element('<div class="togglebutton"><label/></div>')
        wrapper.find('label').append(newElem)
        [newElem, wrapper, no]

    buttonControlGenerator: (name, desc) =>
        newElem = angular.element("<button class=\"btn btn-default\" />")
        newElem.text @gettext('Apply')
        [newElem, newElem, no]

    numberControlGenerator: (name, desc) =>
        newElem = @defaultControlGenerator(name, desc)
        newElem.attr("type", "number")
        if desc.validations.min_value?
            newElem.attr("min", desc.validations.min_value)
        if desc.validations.max_value?
            newElem.attr("max", desc.validations.max_value)
        if desc.validations.step?
            newElem.attr("step", desc.validations.step)
        else
            newElem.attr("step", "any")
        newElem

    rangeControlGenerator: (name, desc) ->
        selectElem = angular.element("<select />")

        for val in [desc.validations.min_value..desc.validations.max_value] by desc.validations.step
            optElem = angular.element("<option>" + val + "</option>")
            optElem.attr("value", val)
            if val == desc.default_value
                optElem.attr 'selected', yes
            selectElem.append(optElem)

        selectElem

    comboboxControlGenerator: (name, desc) ->
        selectElem = angular.element("<select />")

        for k, val of desc.metadata.choices
            optElem = angular.element("<option>" + val + "</option>")
            optElem.attr("value", k)
            if k == desc.default_value
                optElem.attr 'selected', yes
            selectElem.append(optElem)

        return selectElem

    # Update fields from settings form with values from seriesData object
    # This is object send in series:start stanza
    updateExpSettings: (seriesData, settingsDescription) ->
        for key, desc of seriesData.initialSettings
            ctrl = $("#settingsForm #" + key)
            if not settingsDescription[key]? or ctrl.length == 0
                console.error "Unknown setting for control: " + key
                continue

            if settingsDescription[key].type == "boolean"
                ctrl[0].checked = desc.value
            else
                ctrl.val(desc.value)
        return true

    # go through settings form, collect data and return it in map
    # @param settingsMap: this map contains description of settings form, was send by experiment server in stanza mode:set
    getSeriesData: (settingsMap) ->
        form = document.getElementsByName('settingsForm')[0]
        settings = {}
        console.warn 'check form'
        console.warn form
        for key, desc of settingsMap
            if desc.type == 'boolean'
                val = form[key].checked
            else if desc.type == 'button'
                val = false
            else
                val = form[key].value
            settings[key] = { value: @convertInputValue(val, desc) }
        return settings

    convertInputValue: (val, desc) ->
        retVal = val
        switch desc.type
            when "interval", "number", "range"
                retVal = parseFloat(val)
                retVal = val if isNaN(retVal)
        retVal

    typeIsArray: Array.isArray || ( value ) -> return {}.toString.call( value ) is '[object Array]'
