'use strict'


###*
* @ngdoc service
* @id mercury.service:xmpp
* @name mercury.xmppService
* @requires mercury.service:config
* @requires mercury.factory:stanza
* @description xmpp client
###
class Xmpp extends Service

    constructor: (configService, Stanza, languageService, gettext) ->
        return new JSJaCClient(configService, Stanza, gettext)

class ConnectionMonitor

    ###*
    * @param {service} configService service used to get server connection data
    * @param {string} user user login
    * @param {string} pswd user password
    * @param {function} successCallback callback executed when reconnect was successful
    * @param {function} failureCallback callback executed when reconnect was not successful
    ###
    constructor: (@configService, @user, @pswd, @successCallback, @failureCallback) ->
        # number of attempts to reconnect
        @retry = 3
        # delay between attempts to reconnect in mili seconds
        @delay = 1000

    reconnect: () =>
        username = @user
        password = @pswd

        console.debug("reconnect attempt: " + @retry)

        # get new server address and try to reconnect
        @configService.xmpp_server.query((server) =>
            try
                @con = new JSJaCWebSocketConnection(
                    httpbase: server.address
                    oDbg: new JSJaCConsoleLogger(4)
                )
                #                @setup()
                @registerHandlers(@con)
                @con.connect(
                    domain: server.domain
                    username: username
                    resource: 'mercury'
                    pass: password
                    register: false
                )
            catch e
                console.error "Error occurred while reconnecting: #{e.toString()}"
                console.debug e
        )

    registerHandlers: (con) =>
        con.registerHandler "onerror", @onError
        con.registerHandler "status_changed", @onStatusChanged
        con.registerHandler "onconnect", @onConnect
        con.registerHandler "ondisconnect", @onDisconnect

    onError: (e) =>
        if @retry == 0
            console.error "Not possible to reasume connection"
            if @failureCallback?
                @failureCallback()
            return
        @retry--
        setTimeout(@reconnect.bind(this), @delay)
        console.debug "error while connection attempt"
        console.debug e

    onStatusChanged: (status) ->
        console.debug "onStatusChanged"
        console.debug status

    onConnect: () =>
        console.log "onConnect"
        #remove handlers from con object - they were present only for reconnect purpose
        @con.unregisterHandler "onerror", @onError
        @con.unregisterHandler "status_changed", @onStatusChanged
        @con.unregisterHandler "onconnect", @onConnect
        @con.unregisterHandler "ondisconnect", @onDisconnect

        # execute callback for successful reconnect - pass new working connection
        @successCallback(@con)
        return

    onDisconnect: () ->
        console.debug "onDisconnect"

class XMPPClient

    constructor: ->
        @handlers = {}

    setup: =>
        console.debug @handlers
        for own event, handlers of @handlers
            for handler in handlers
                @registerHandler(handler.originalEvent, handler.modified)

        @registerIQGet('query', NS_VERSION, @handleIqVersion)
        @registerIQGet('query', NS_TIME, @handleIqTime)

    on: (event, handler, terminate = no) ->
        console.debug "trying to register #{handler} on #{event}"
        #TODO zmienić tak, by uniezależnić się od JSJaCC
        evt = if event instanceof Array then event[0] else event
        arr = @handlers[evt] or []
        fun = ->
            handler(arguments...)
            terminate
        arr.push {
            original: handler
            modified: fun
            # copy event object
            originalEvent: event.slice(0)
        }
        @handlers[evt] = arr
        console.debug "@handlers[#{evt}] = #{@handlers[evt]}"
        try
            console.debug "on(#{event}, #{fun})"
            @registerHandler event, fun

    off: (event, handler) ->
        console.debug "trying to unregister #{handler} on #{event}"
        arr = @handlers[event]
        console.debug "@handlers[#{event}] = #{arr}"
        if arr
            console.debug "trying to find #{handler} on #{event}"
            for item, idx in arr
                if item.original == handler
                    handler = item.modified
                    arr[idx..idx] = []
                    console.debug "found on index #{idx}"
                    console.debug "changing handler from #{item.original} to #{handler}"
                    break
        try
            console.debug "off(#{event}, #{handler})"
            @unregisterHandler(event, handler)

    registerHandler: ->
    unregisterHandler: ->
    registerIQGet: ->

    handleIqVersion: ->
    handleIqTime: ->

    connect: (address, server, username, password) ->
    disconnect: ->
    sendMsg: (msg, sendTo) ->
    connectToGroupchat: (chat, alias) ->


class JSJaCClient extends XMPPClient

    initErrorMessages: =>
        @errorMessages = {
            "default": @gettext('''Connection error occurred.
Check your network connection and whether you not have the laboratory opened in another tab or browser.
Refresh this page after solving the problem.'''),
            "remote-stream-error": @gettext('''Login conflict.
Check that you are not logged in another tab or browser.'''),
            "service-closed": @gettext('Connection to server error occurred - laboratory is unavailable.')
        }

    constructor: (@configService, @Stanza, @gettext)->
        super
        @initErrorMessages()
        @oDbg = new JSJaCConsoleLogger(4)
        @connected = false
        # id timeout-u dla ostatniej wyslanej wiadomosci, jesli nic sie nie dzieje przez zadany czas to wysylany jest ping
        @lastMsgTimeoutId = 0
        # id timeout-u dla wyslanego ping-a aby anulowac handler ktory dziala jesli jest brak odpowiedzi od serwera
        @pingTimeoutId = 0
        @status = 'ready'
        console.log "STATUS: #{@status}"

        @connectionLostCallback = @noOperationFun
        @connectionResumedCallbacks = []
        @connectionNotResumedCallback = @noOperationFun

        @on('onerror', (e) =>
            condition = e.firstChild.nodeName
            text = "An error occured:\n" + ("Code: " + e.getAttribute('code') + "\nType: " + e.getAttribute('type') +
                "\nCondition: " + condition + "\nstatus: " + @con.status())
            console.error text

            if condition == "service-closed" and @reconnect()
                return
            errorMsg = if @errorMessages[condition]? then @errorMessages[condition] else @errorMessages["default"]
#            alert errorMsg
            console.error errorMsg
            if @con.connected()
                console.warn("DISCONETCTING - onError")
                @con.disconnect()
            if "remote-stream-error" == condition
                # po tym bledzie nie ma generacji tego eventu
                @con._handleEvent('ondisconnect')
        )

        @on('status_changed', (status) =>
            @status = status
            @oDbg.log("status changed: " + status)
        )

        @on('onconnect', =>
            console.debug 'presence'
            @connected = yes
            @con.send(new JSJaCPresence())
        )

        @on('ondisconnect', =>
            console.warn("on ondisconnect - xmpp.coffe")
            @connected = no
        )

    # Start reconnect procedure if connection was lost. Execute connection lost callback
    # and initiate ConnectionMonitor to reconnect to server. If no exceptions occure returns true.
    reconnect: () =>
        console.info "RECONECTING " + @username
        @connectionLostCallback()
        try
            rr = new ConnectionMonitor(@configService, @username, @password, @reconnectSuccess, @connectionNotResumedCallback)
            rr.reconnect()
            return true
        catch
            return false

    # Callback executed if reconnect procedure finished with success - connection
    # with server is resumed and running.
    reconnectSuccess: (newConnection) =>
        console.info "Connection resumed"
        # replace old connection with new one
        @con = newConnection
        # init handlers for new connection - add all of them which were registered for old connection
        @setup()
        # execute callback that reconnect was successful
        for c in @connectionResumedCallbacks
            c()

    registerHandler: (event, handler) =>
        if not @con?
            console.info "Connection not present, can not register handler for event: " + event
            console.debug handler
            return
        console.debug "registering #{handler} on #{event}"
        if Array.isArray event
            event.push handler
            @con.registerHandler event...
        else
            @con.registerHandler event, handler

    unregisterHandler: (event, handler) =>
        console.debug "deregistering #{handler} on #{event}"
        @con.unregisterHandler(event, handler)


    ###handleMessage: (oJSJaCPacket) =>
        text = 'Received Message from ' + oJSJaCPacket.getFromJID() + ':'
        text += oJSJaCPacket.getBody().htmlEnc()
        alert(text)###

    ###handlePresence: (oJSJaCPacket) =>
        if !oJSJaCPacket.getType() && !oJSJaCPacket.getShow()
            text = oJSJaCPacket.getFromJID() + ' has become available.'
        else
            text =  oJSJaCPacket.getFromJID() + ' has set his presence to '
            if oJSJaCPacket.getType()
                text += oJSJaCPacket.getType() + '.'
            else
                text += oJSJaCPacket.getShow() + '.'
            if oJSJaCPacket.getStatus()
                text += ' (' + oJSJaCPacket.getStatus().htmlEnc() + ')'
        alert(text)###

    handleIqVersion: (iq) =>
        @con.send(iq.reply([iq.buildNode('name', 'jsjac simpleclient'), iq.buildNode('version', JSJaC.Version), iq.buildNode('os', navigator.userAgent)]))


    handleIqTime: (iq) =>
        now = new Date()
        @con.send(iq.reply([iq.buildNode('display', now.toLocaleString()), iq.buildNode('utc', now.jabberDate()), iq.buildNode('tz', now.toLocaleString().substring(now.toLocaleString().lastIndexOf(' ') + 1))]))

    ###
        Send ping messages every X seconds if connection is idle.
        If result is not received trigger reconnect scenario.
    ###
    sendPing: () =>
        iiq = new JSJaCIQ()
        iiq.setTo(@con.domain)      # ping msg goes to the server itself
        iiq.setType('get')
        pp = iiq.getDoc().createElementNS('urn:xmpp:ping','ping')
        pp.setAttribute('xmlns', 'urn:xmpp:ping')
        iiq.getNode().appendChild(pp)

        @con.sendIQ(iiq, {result_handler: @onPingResult})
        # wait for result of ping
        @pingTimeoutId = setTimeout(@onPingTimeout, 3000)
        # set another ping to be sent in case of still idle connection
        @lastMsgTimeoutId = setTimeout(@sendPing, @configService.pingTimeout)

    onPingResult: () =>
        console.warn "onPingResult " + @pingTimeoutId
        clearTimeout(@pingTimeoutId)

    onPingTimeout: () ->
        #TODO mozna sie zastanowic czy wywolywac reconnect
        console.error 'PING timout occured - server is not responding to ping messages'

    prepareSendTo: (sendTo) =>
        if sendTo.indexOf('@') == -1
            return "#{sendTo}@#{@con.domain}"
        else
            return sendTo


    ################# PUBLIC API ################


    ###*
    * @doc method
    * @methodOf mercury.service:xmpp
    * @name xmppService#connect
    * @param {string} username user login
    * @param {string} password user password
    * @param {boolean} register registers new user if flag is `true`
    * @description connects to xmpp server
    * @return {boolean} `true` if connection established, `false` otherwise
    ###
    connect: (username, password, register = false) =>
        console.log "STATUS: #{@status}"
        if @status == 'connecting' or @status == 'disconnecting'
            return false
        @status = 'connecting'
        #TODO zakladamy ze pobranie danych o serwerze dziala, nalezy tutaj dodac callback
        @configService.xmpp_server.query((server) =>
            try
                @con = new JSJaCWebSocketConnection(
                    httpbase: server.address
                    oDbg: @oDbg
                )
                @setup()
                @con.connect(
                    domain: server.domain
                    username: username
                    resource: 'mercury'
                    pass: password
                    register: register
                )
                # store for reconnect purpose
                @username = username
                @password = password
            catch e
                console.error "Error occured: #{e.toString()}"
                alert(@gettext('Connection error') + ": #{e.toString()}")
                console.log e
                @status = 'ready'
        )
        return false

    ###*
    * @doc method
    * @methodOf mercury.service:xmpp
    * @name xmppService#send
    * @param {object} packet data packet to send
    * @description sends stanza to xmpp server
    ###
    send: (packet) =>
        clearTimeout(@lastMsgTimeoutId)
        @con.send(packet)
        @lastMsgTimeoutId = setTimeout(@sendPing, @configService.pingTimeout)
        return true

    ###*
    * @doc method
    * @methodOf mercury.service:xmpp
    * @name xmppService#prepareMessage
    * @param {string} sendTo message recipient
    * @param {boolean} groupchat specify wheter message should be send to groupchat
    * @description prepares message stanza
    * @return {object} prepared message
    ###
    prepareMessage: (sendTo, groupchat) =>
        sendTo = @prepareSendTo sendTo
        msg = new JSJaCMessage()
        msg.setTo new JSJaCJID(sendTo)
        if groupchat
            msg.setGroupchat true
        return msg

    ###*
    * @doc method
    * @methodOf mercury.service:xmpp
    * @name xmppService#sendMsg
    * @param {string} msg message to send
    * @param {string} sendTo message recipient
    * @param {boolean} groupchat specify wheter message should be send to groupchat
    * @return {boolean} `true` if message is send, `false` otherwise
    * @description sends message stanza to specified recipient
    ###
    sendMsg: (msg, sendTo, groupchat) =>
        if !msg and !sendTo
            return false

        try
            oMsg = @prepareMessage sendTo, groupchat
            oMsg.setBody msg
            return @send oMsg
        catch e
            console.error "Error: #{e.message}"
            return false

    ###*
    * @doc method
    * @methodOf mercury.service:xmpp
    * @name xmppService#sendLabMsg
    * @param {object} labdata {@link mercury.factory:stanza#LabData labdata object} to send
    * @param {string} sendTo message recipient
    * @param {boolean} groupchat specify wheter message should be send to groupchat
    * @return {boolean} `true` if message is send, `false` otherwise
    * @description sends message stanza to specified recipient
    ###
    sendLabMsg: (labdata, sendTo, groupchat) =>
        if !labdata and !sendTo
            return false

        sendTo = @prepareSendTo sendTo

        try
            oMsg = @prepareMessage sendTo, groupchat
            if not (labdata instanceof @Stanza.LabData)
                labdata = new @Stanza.LabData(labdata)
            oMsg.setLabData labdata
            return @send oMsg
        catch e
            console.error "Error: #{e.message}"
            return false

    ###*
    * @doc method
    * @methodOf mercury.service:xmpp
    * @name xmppService#connectToGroupchat
    * @param {string} chat chat identifier
    * @param {string} alias user alias
    * @description connects to groupchat
    ###
    connectToGroupchat: (chat, alias) =>
        if !chat
            return false

        chat = @prepareSendTo chat
        if !alias
            alias = @con.username

        prs = new JSJaCPresence()
        prs.setTo new JSJaCJID("#{chat}/#{alias}")
        return @send prs

    ###*
    * @doc method
    * @methodOf mercury.service:xmpp
    * @name xmppService#leaveGroupchat
    * @param {string} chat chat identifier
    * @param {string} alias user alias
    * @description leaves groupchat
    ###
    leaveGroupchat: (chat, alias) =>
        p = new JSJaCPresence()
        p.setType("unavailable")
        if !alias
            alias = @con.username
        p.setTo new JSJaCJID("#{chat}/#{alias}")
        return @send p

    ###*
    * @doc method
    * @methodOf mercury.service:xmpp
    * @name xmppService#disconnect
    * @description disconnects from server
    ###
    disconnect: =>
        p = new JSJaCPresence()
        p.setType("unavailable")
        @con.send(p)
        @con.disconnect()

    onConnectionLost: (callback) =>
        @connectionLostCallback = callback

    onConnectionResumed: (callback) =>
        @connectionResumedCallbacks.push(callback)

    onConnectionNotResumed: (callback) =>
        @connectionNotResumedCallback = callback

    isConnectedToServer: () =>
        return @connected

    noOperationFun: () ->
        console.debug "no operation"
