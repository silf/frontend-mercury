'use strict'

###*
* @ngdoc service
* @id mercury.service:language
* @name mercury.languageService
* @description language configuration
###
class Language extends Service

    constructor: (@gettextCatalog) ->
        lang = navigator.language || navigator.userLanguage || 'en'
        @setLanguage lang

    setLanguage: (lang) =>
        if lang
            @currentLanguage = lang
            @gettextCatalog.setCurrentLanguage @currentLanguage
            console.info "setting language: #{@currentLanguage}"

    setLanguageIfNotSet: (lang) =>
        if not @currentLanguage
            @setLanguage lang

    currentLangCode: =>
        if @currentLanguage?.length > 2
            return @currentLanguage.substr(0, 2)
        return @currentLanguage ? 'en'
