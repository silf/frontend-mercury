'use strict'

###*
* @ngdoc service
* @id mercury.factory:stanza
* @name mercury.Stanza
* @requires mercury.factory:utils
* @description provides additional stanza extensions
###
class Stanza extends Factory

    constructor: (Utils) ->

        ###*
        * @doc class
        * @propertyOf mercury.factory:stanza
        * @name Stanza#LabData
        * @description class element extending message stanza by
        * LabData property which contains data used in
        * internal communication between client and experiment server
        ###
        class LabData

            constructor: (config) ->
                @id = config.id or Utils.uuid()
                @type = config.type
                @xmlns = config.xmlns
                @data = config.data

            getNode: (doc) =>
                try
                    node = doc.createElementNS @xmlns, 'labdata'
                catch e
                    node = doc.createElement 'labdata'
                    node.setAttribute 'xmlns', @xmlns
                node.setAttribute 'type', @type
                node.setAttribute 'id', @id
                if @data
                    node.appendChild doc.createTextNode(angular.toJson @data)
                return node

            @parse: (node) ->
                config =
                    id: node.getAttribute('id')
                    type: node.getAttribute('type')
                    xmlns: node.getAttribute('xmlns')
                    data: if node.childNodes.length then angular.fromJson node.childNodes.item(0).nodeValue else null
                new LabData(config)


        ###*
        * @doc class
        * @propertyOf mercury.factory:stanza
        * @name Stanza#Message
        * @description extended message stanza class whith
        * {@link mercury.factory:stanza#LabData LabData} property
        ###
        JSJaCMessage.prototype.setLabData = (labData) ->
            @getNode().appendChild labData.getNode(@getDoc())

        JSJaCMessage.prototype.getLabData = ->
            ld = @getNode().getElementsByTagName('labdata')
            if ld
                return LabData.parse ld.item(0)
            return null

        JSJaCMessage.prototype.setGroupchat = (groupchat = true) ->
            if not groupchat
                @getNode().removeAttribute('type')
            else
                @getNode().setAttribute 'type', 'groupchat'
            return @


        return {
            Message: JSJaCMessage
            LabData: LabData
        }
