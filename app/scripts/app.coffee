'use strict'

###*
* @ngdoc object
* @name mercury
* @description Main module
###
class Mercury extends App
    constructor: ->
        return [
            'ngRoute',
            'ngSanitize',
            'ngResource',
            'ui.bootstrap',
            'Constants',
            'mercury-templates',
            'angular-markdown',
            'gettext'
        ]
