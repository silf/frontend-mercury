'use strict'

###*
* @ngdoc controller
* @id mercury.controller:seriesResultRepository
* @name mercury.controller:seriesResultRepositorytCtrl
* @requires ng.$scope
* @requires mercury.factory:experiment - to be present in the parent scope
* @requires mercury.service:config
* @description Repository for series id used to generate links for download of series results
###
class SeriesResultRepository extends Controller

    constructor: (@$scope, @configService) ->
        @seriesNo = 0
        @seriesInfo = []
        @currentSeriesInfo = null
        @urlTemplate = @configService.expResultsUrl
        @$scope.$on('connectedToExp', @registerExperimentHandlers)
        @$scope.seriesInfo = @seriesInfo

    ###
    Registers event handlers.
    ###
    registerExperimentHandlers: =>
        experiment = @$scope.experimentCtrl
        experiment.onStartSeries @handlerStartSeries, null, null
        experiment.onStopSeries @handlerStopSeries
        experiment.onExperimentStop @handlerStopSeries
        console.debug 'SeriesResultRepository.registerExperimentHandlers'

    handlerStartSeries: (seriesData) =>
        @seriesNo += 1
        seriesId = seriesData.seriesId
        name = if seriesData.metadata then seriesData.metadata.label else "seria #{@seriesNo}"
        @currentSeriesInfo = new SeriesInfo(name, seriesId, @urlTemplate)

    handlerStopSeries: =>
        if @currentSeriesInfo == null
            return
        @currentSeriesInfo.seriesStoped()
        @$scope.$apply =>
            @seriesInfo.push(@currentSeriesInfo)
        @currentSeriesInfo = null


class SeriesInfo

    constructor: (@name, @seriesId, @urlTemplate) ->
        @startDate = new Date()
        @endDate = null

    seriesStoped: ->
        @endDate = new Date()

    getResultUrl: ->
        return @urlTemplate.replace('DATA_SERIES_ID', @seriesId)

    getResultName: ->
        return @name + " " + @formatDates(@startDate, @endDate)

    formatDates: (startDate, finishDate) ->
        stDate = ''
        endDate = ''
        now = new Date()
        useDay = startDate.getDate() != finishDate.getDate() || now.getDate() != startDate.getDate()
        if (useDay)
            stDate += startDate.getDate() + '.' + (startDate.getMonth() + 1) + ' '
            endDate += finishDate.getDate() + '.' + (finishDate.getMonth() + 1) + ' '
        stDate += @pad(startDate.getHours()) + ':' + @pad(startDate.getMinutes())
        endDate += @pad(finishDate.getHours()) + ':' + @pad(finishDate.getMinutes())

        return stDate + ' - ' + endDate

    pad: (numVal) ->
        return if (numVal >= 10) then '' + numVal  else '0' + numVal
