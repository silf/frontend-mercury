'use strict'

###*
* @ngdoc controller
* @id mercury.controller:genericExperiment
* @name mercury.controller:genericExperimentCtrl
* @requires ng.$scope
* @requires ng.$document
* @requires ng.$compile
* @requires ng.$routeParams
* @requires mercury.factory:experiment
* @requires mercury.factory:utils
* @requires mercury.service:config
* @requires mercury.service:experimentUtil
* @description experiment controller (common for all experiments)
###
class GenericExperiment extends Controller

    ###
    Map contains possible values for seriesStatus flag indicating in which state is series measurement.
    Can be: notWorking, inProgress or onFirstResult
    ###
    seriesStatusValues =
        # series measurement is in progress - meaning that operator has set some starting data and experiment is in progress
        inProgress: "inProgress",
        # this code means that right now there is no measurement but if result stanza will be received, state must be switched to 'inProgress'
        onFirstResult: "onFirstResult",
        # there is no measurement going on right now
        notWorking: "notWorking"

    constructor: (@$scope, @Experiment, @$document, @$compile, $routeParams, @Utils, @configService, @$modal, @$timeout, @gettext, @experimentUtilService) ->

        @experimentConfig = null
        @experimentName = $routeParams.name

        # experiment controller is created in bindScope method
        @experiment = null

        @currentResultDesc = null
        @chartData = []
        @currentDataPoints = []
        @seriesNo = 0
        @seriesId = null
        @modalInstance = null
        # for operator skip updating settings panel if he triggered start of measurement session
        @skipUpdateSettings = false

        @bindScope()


    ###
    Maps several class properties and methods
    to $scope properties and methods
    accessible in html view.
    ###
    bindScope: =>
        @$scope.status = @gettext('disconnected')
        @$scope.powerLabel = @gettext('run experiment')
        @$scope.activeExpChat = true
        @$scope.selectedExp = null
        @$scope.modes = null
        @$scope.seriesStatus = seriesStatusValues.notWorking
        @$scope.seriesInProgress = seriesStatusValues.inProgress
        @configService.experiments.query((data) =>
            @experimentConfig = (data.filter (exp) => exp.codename == @experimentName)[0]
            if @experimentConfig
                @$scope.experimentName = @experimentUtilService.getExperimentDisplayName(@experimentConfig.display_name, @experimentConfig.codename)
                @$scope.cameraUrl = @experimentConfig.camera_url
                @$scope.docsUrl = @experimentConfig.docs_url
                @$scope.experimentConfig = @experimentConfig

                # calculate chat url
                chatUrl = @experimentConfig.url.split("@")
                @$scope.expChatUrl = chatUrl[0] + "-chat@" + chatUrl[1]
                @$scope.generalChat = @configService.generalRoomName + "@" + chatUrl[1]

                @experiment = new @Experiment(@experimentConfig.url)
                # bind experiment controller to scope so that child scopes can also use this controller
                @$scope.experimentCtrl = @experiment
        )
        @$scope.chartData = @chartData

        @$scope.togglePower = @togglePower
        @$scope.connect = @connect
        @$scope.setMode = @setMode
        @$scope.stopExperiment = @stopExperiment
        @$scope.startSeries = @startSeries
        @$scope.stopSeries = @stopSeries
        @$scope.chartType = 'scatter'

    isSeriesInProgress: =>
        @$scope.seriesStatus == seriesStatusValues.inProgress

    ###
    Clears all experiment data.
    Used when user disconnects from experiment.
    @param {boolean} clearModeData clears info about modes
    ###
    clearExperimentData: (clearModeData) =>
        if clearModeData? and clearModeData == true
            @$scope.selectedExp = null
            @$scope.modes = null
            @$scope.currentMode = null

        @Utils.unregisterOnChangeEventForSettingsForm()
        @seriesNo = 0
        @seriesId = null
        @$scope.settings = null
        @$scope.settingsToDisplay = null
        @$scope.seriesStatus = seriesStatusValues.notWorking

        if @currentResultDesc
            for key, desc of @currentResultDesc
                if desc.class != "chart"
                    delete @$scope[key]
            @currentResultDesc = null

        # TODO: to think how to put it in chart directive
        #if @chart
        #    @$document.find('#chartContainer').children().remove()
        @currentDataPoints = []
        @chartData = []
        @$scope.chartData = @chartData
        @$document.find('#resultContainer').children().remove()


    ###
    Retrieves experiment modes.
    In case of single available mode this mode is
    automatically chosen.
    ###
    getModes: =>
        @experiment.getModes (data) =>
            modeD = ({key: k, value: v} for k, v of data)

            if modeD.length > 1
                modeD.sort((l, r) ->
                    comResult = 0
                    if l.value.order? and r.value.order?
                        comResult = l.value.order - r.value.order
                    # sort by label if order is not present or is equal
                    if comResult == 0
                        comResult = l.value.label.localeCompare(r.value.label)

                    return comResult
                )
            @$scope.$apply =>
                @$scope.modes = modeD
            if modeD.length == 1
                @setMode(modeD[0].key)
        , (errorMsg) =>
            console.error errorMsg
            @handlerDisconnect()
            alert(errorMsg)


    ###
    Sorts settings controls.
    ###
    getSettingsAsSortedArray: (sett) ->
        settArr = []
        for k, desc of sett
            desc.name = k
            settArr.push(desc)

        settArr.sort((l, r) ->
            comResult = 0
            if l.order? and r.order?
                comResult = l.order - r.order
            # sort by label if order is not present or is equal
            if comResult == 0
                comResult = l.metadata.label.localeCompare(r.metadata.label)

            return comResult
        )

        return settArr


    ############ Event Handlers #############

    ###
    Experiment disconnect event handler.
    Clears experiment data.
    ###
    handlerDisconnect: =>
        @handlerExperimentStop()
        @$scope.$apply (scope) =>
            scope.experiment_connected = no
            scope.status = @gettext('disconnected')

    handlerConnectionLost: =>
        # show overlay with info that connection was lost
        @modalInstance = @$modal.open({
            templateUrl: 'conectionLostWarnning',
            keyboard: false,
            backdrop: 'static',
#            controller: genericExperimentCtrl
        })

    handlerConnectionResumed: =>
        # close overlay with connection lost info
        @modalInstance.close()

    handlerConnectionNotResumed: =>
        # TODO zapisz wyniki jesli sa jakies dostepne
        # close UI if connection was not resumed
        @handlerDisconnect()
        alert "Cannot establish connection with server."
        # close overlay with connection lost info
        @modalInstance.close()

    ###
    Handler for experiment set mode
    Delete values for old mode and render GUI for new mode
    ###
    handlerSetMode: (data) =>
        # wyzeruj tagi z wynikami
        @clearExperimentData(false)
        # zrenderuj czesc z wynikami
        @onResultDescription(data.resultDescription)
        @currentResultDesc = data.resultDescription
        # zrenderuj czesc do sterowania eksperymentem
        console.debug data.settings

        @Utils.generateControlForSettings(data.settings, @isObserver())
        sortedSettings = @getSettingsAsSortedArray(data.settings)
        @$scope.$apply =>
            @$scope.settings = data.settings
            @$scope.settingsToDisplay = sortedSettings
        #$scope.currentMode = mode
        #add validation only for OPERATOR
        if not @isObserver()
            @Utils.registerOnChangeEventForSettingsForm(data.settings, @experiment, @$scope)
        @$timeout ->
            $.material.init()
        false

    ###
    Experiment result event handler.
    Updates view with current data.
    ###
    handlerResult: (data) =>
        for key, desc of @currentResultDesc
            resData = data[key]
            if not resData?
                continue
            if desc.class == "interval-indicator" or desc.class == "integer-indicator"
                @$scope.$apply =>
                    @$scope[key] = resData.value
            else if desc.class == "chart"
                if resData.pragma == 'replace'
                    @currentDataPoints[..] = []
                for point in resData.value
                    @currentDataPoints.push {
                        x: point[0]
                        y: point[1]
                    }
                @$scope.$apply =>
                    @currentDataPoints
        if @$scope.seriesStatus == seriesStatusValues.onFirstResult
            # this is case when operator has resumed connection which was lost, first result stanza switches off start button
            @$scope.$apply =>
                @$scope.seriesStatus = seriesStatusValues.inProgress

    ###
    Experiment start series event handler.
    Creates new data series.
    ###
    handlerStartSeries: (seriesData) =>
        if (@isObserver() || not @skipUpdateSettings)
            # update settings for observer or for operator only if chatbot is performing experiment
            @Utils.updateExpSettings(seriesData, @$scope.settings)

        if not @isObserver()  # for observer they should be already disabled
            @Utils.disableNonLiveSettings(@$scope.settings)

        @skipUpdateSettings = false
        @seriesId = seriesData.seriesId
        console.info "series #{@seriesId} started"
        @$scope.$apply =>
            @$scope.seriesStatus = seriesStatusValues.inProgress
        @seriesNo += 1
        @currentDataPoints = []
        @chartData.push(
            type: @$scope.chartType
            showInLegend: yes
            name: if seriesData.metadata then seriesData.metadata.label else "seria #{@seriesNo}"
            dataPoints: @currentDataPoints
        )
        @$scope.$apply =>
            @chartData

    ###
    Experiment error when starting series event handler.
    Display error messages for fields on settings form
    ###
    handlerStartSeriesWithError: (errors, callbackData) =>
        if not @isObserver()
            @Utils.onSettingsCheckWithErrors(errors, callbackData)

    ###
    Experiment stop series event handler.
    ###
    handlerStopSeries: =>
        @seriesId = null
        @$scope.$apply =>
            @$scope.seriesStatus = seriesStatusValues.notWorking
            # clear validation execution flag
            for key, desc of @$scope.settings
                desc.validations.execute = false
        if not @isObserver()  # for observer they should be still disabled
            @Utils.enableNonLiveSettings(@$scope.settings)

    ###
    Stop experiment event handler - meaning that reservation has finished or operator stopped whole experiment
    ###
    handlerExperimentStop: =>
        #TODO zapisz wyniki eksperymentu jesli jakies dostepne
        #this will be executed in async so run it inside of apply
        @$scope.$apply =>
            @clearExperimentData(true)

    ###
    Experiment error event handler.
    Displays error message for user.
    ###
    handlerError: (errors) ->
        for error in errors
            console.error error.metadata.message
            alert error.metadata.message

    ###
    Experiment operator event handler.
    Retrieves experiment modes.
    ###
    handlerOperator: =>
        console.warn "I'm an OPERATOR!    ver=" + @experiment.protocolVersion
        @$scope.$apply =>
            @$scope.status = @gettext('operator')
        if @$scope.settings?
            # If settings are present it means that operator lost connection with server for brief time.
            # Restore GUI to proper state for operator
            # After connection was resumed all controls are in read only mode for observer. When becoming operator enable all of them
            @Utils.enableSettings(@$scope.settings, @experiment, @$scope)
            @$scope.$apply =>
                # enable start series button, and if series is in progress first result stanza will disable it
                @$scope.seriesStatus = seriesStatusValues.onFirstResult
            return
        @handlerExperimentStop()
        @getModes()

    ###
    Experiment observer event handler.
    ###
    handlerObserver: =>
        console.log "I'm an OBSERVER!"
        @handlerExperimentStop()
        @$scope.$apply =>
            @$scope.status = @gettext('observer')

    ###
    Registers above event handlers.
    ###
    registerExperimentHandlers: =>
        console.info "registerExperimentHandlers"
        @experiment.onDisconnect @handlerDisconnect
        @experiment.onConnectionLost @handlerConnectionLost
        @experiment.onConnectionResumed @handlerConnectionResumed
        @experiment.onConnectionNotResumed @handlerConnectionNotResumed
        @experiment.onSetMode @handlerSetMode
        @experiment.onResult @handlerResult
        @experiment.onStartSeries @handlerStartSeries, @handlerStartSeriesWithError, @$scope
        @experiment.onStopSeries @handlerStopSeries
        @experiment.onExperimentStop @handlerExperimentStop
        @experiment.onError @handlerError
        @experiment.onBecomeOperator @handlerOperator
        @experiment.onBecomeObserver @handlerObserver


    ###
    Generates controls for presentation of experiment results,
    based on metadata present in the resultDescription argument.
    Also $scope is updated and for each result appropriate variable is created in $scope.
    ###
    onResultDescription: (resultDescription) =>
        @$scope.chartTitle = null
        for key, desc of resultDescription
            # 1 dzila
            if desc.class == "interval-indicator" or desc.class == "integer-indicator"
                mainContener = angular.element('<div id="result:' + key + '"></div>')
                mainContener.append(angular.element('<span>' + desc.metadata.label + '</span>'))
                labelElem = angular.element(@$compile('<label>{{' +  key + '}}</label>')(@$scope))
                mainContener.append(labelElem)
                @$document.find('#resultContainer').append(mainContener)
                #TODO to ponizsze przenies do applay w metodzie wyzej
                @$scope[key] = '-'
                # tez dziala ponizsza opcja
#                $compile('<p>{{' + key + '}}</p>')($scope, (el, scope) ->
#                    angular.element($document.find('body')[0]).append el
#                )
            else if desc.class == "chart"
                @$scope.chartXLabel = desc.metadata['chart.axis.x.label']
                @$scope.chartYLabel = desc.metadata['chart.axis.y.label']
                @$scope.chartTitle = desc.metadata.label || ''
            else
                console.warn("Unknown result control class: " + desc.class)
        camElem = @$document.find("camera")[0]

        if (@$scope.chartTitle != null)
            @$document.find("#settingsPanel").append(camElem)
        else
            @$document.find("#resultsPanel").prepend(camElem)
        # call apply to refresh GUI
        @$scope.$apply()


    ################### PUBLIC API ####################

    ###*
    * @doc method
    * @methodOf mercury.controller:genericExperiment
    * @name genericExperimentCtrl#togglePower
    * @description toggle connection status
    ###
    togglePower: =>
        if @$scope.experiment_connected
            @stopExperiment()
        else
            @connect()

    ###*
    * @doc method
    * @methodOf mercury.controller:genericExperiment
    * @name genericExperimentCtrl#connect
    * @description connects to experiment an retieves experiment modes
    ###
    connect: =>
        if not @experimentConfig
            alert @gettext('Choose experiment')
            return
        @registerExperimentHandlers()
        @$scope.powerProcessing = true
        @$scope.experiment_connected = @experiment.connect(@$scope.login)
        @$scope.powerProcessing = false
        @$scope.powerLabel = @gettext('finish experiment')
        if (@$scope.experiment_connected)
            # broadcast that experiment is connected to child scopes
            @$scope.$broadcast('connectedToExp')
        ###setInterval ->
            experiment.getState((state) ->
                $scope.$apply(
                    $scope.state = state
                )
            )
        , 3000###


    ###*
    * @doc method
    * @methodOf mercury.controller:genericExperiment
    * @name genericExperimentCtrl#stopExperiment
    * @description Stops current measurement, clear user interface and disconnects from experiment.
    ###
    stopExperiment: =>
        @$scope.powerProcessing = true
        #TODO zrob tez na onUnLoad
        if @isSeriesInProgress()
            @stopSeries()
        @experiment.stop()
        @experiment.disconnect()
        @$scope.powerProcessing = false
        @$scope.powerLabel = @gettext('run experiment')

    ###*
    * @doc method
    * @methodOf mercury.controller:genericExperiment
    * @name genericExperimentCtrl#setMode
    * @param {string} mode chosen mode
    * @description sets experiment mode chosen by user
    ###
    setMode: (mode) =>
        if @isSeriesInProgress()
            alert(@gettext('Stop measurement to change experiment mode'))
            #TODO nie zmieni to moda, ALE wcisniecie guzika update-uje model i zmiania sie wcisniety guzik!!
            return
#        @experiment.setMode mode, @handleSetMode
        @experiment.setMode mode
        false

    # return true if user is observer
    isObserver: () =>
        return @$scope.status == @gettext('observer')

    ###*
    * @doc method
    * @methodOf mercury.controller:genericExperiment
    * @name mercury.controller:genericExperiment#startSeries
    * @description starts new experiment series with
    * current settings from settings panel
    ###
    startSeries: =>
        @skipUpdateSettings = true
        settings = @Utils.getSeriesData @$scope.settings
        @experiment.startSeries settings


    ###*
    * @doc method
    * @methodOf mercury.controller:genericExperiment
    * @name genericExperimentCtrl#stopSeries
    * @description stops current series
    ###
    stopSeries: =>
        @experiment.stopSeries @seriesId
