'use strict'

class Client extends Controller

    constructor: ($scope, xmppService, Stanza) ->

        $scope.labdata =
            xmlns: 'silf:mode:get'
            type: 'query'
            data: ''

        $scope.sendTo = 'fakeroom@muc.pip.ilf.edu.pl'

        $scope.sendMsg = ->
            if $scope.msg
                xmppService.sendMsg $scope.msg, $scope.sendTo, $scope.groupChat
            else
                xmppService.sendLabMsg $scope.labdata, $scope.sendTo, $scope.groupChat


        xmppService.on('onconnect', ->
            $scope.$apply(
                appendToLogs 'operation', 'Połączono'
            )
        )


        xmppService.on('ondisconnect', ->
            appendToLogs 'operation', 'Rozłączono'
        )

        xmppService.on('presence', (packet) ->
            console.debug packet
            if !packet.getType() && !packet.getShow()
                text = packet.getFromJID() + ' stał się dostępny'
            else
                text =  packet.getFromJID() + " zmienił swoją obecność na #{if packet.getType() then packet.getType() else packet.getShow()}"
                if packet.getStatus()
                    text += ' (' + packet.getStatus().htmlEnc() + ')'
            $scope.$apply(
                appendToLogs 'operation', text
            )
        )

        xmppService.on('message_in', (packet) ->
            text = "Wiadomość od #{packet.getFromJID()}: "
            text += packet.getBody().htmlEnc()
            $scope.$apply(
                appendToLogs 'received', text
            )
            console.debug packet.getLabData()
        )

        xmppService.on('message_out', (packet) ->
            text = "Wiadomość do #{packet.getToJID()}: "
            text += packet.getBody().htmlEnc()
            appendToLogs 'sent', text
        )

        $scope.logs = []
        appendToLogs = (type, command) ->
            $scope.logs = [{
               type: type,
               command: command
            }].concat $scope.logs

        $scope.fake_connect = ->
            alias = prompt "Podaj alias"
            xmppService.connectToGroupchat 'fakeroom@muc.pip.ilf.edu.pl', alias
