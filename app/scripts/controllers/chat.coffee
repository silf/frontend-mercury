'use strict'

###*
* @ngdoc controller
* @id mercury.controller:xmppChat
* @name mercury.controller:xmppChatCtrl
* @requires ng.$scope
* @requires ng.$attrs
* @requires mercury.service:xmpp
* @description Chat controller
* Assumption is that parent scope contains data:
*    login - user name used in chat
*    variable with name defined in attribute chat-url-var. Example:
*    chat-url-var="someVar" means that there is $scope.someVar variable defined
*    in parent scope and this variable holds url for chat room
###
class XmppChat extends Controller

    constructor: (@$scope, $attrs, @xmppService, @experimentContextService, @configService) ->
        @chatUrl = @$scope[$attrs.chatUrlVar]
        @isGeneralChat = @chatUrl.indexOf(@configService.generalRoomName + "@") != -1
        @onConnectCallback = null
        if not @chatUrl?
            console.error "Chat url is undefined, there is no variable " + $attrs.chatUrlVar + " in $scope"
            return

        # bind things to scope
        @$scope.send = @send
        @$scope.messages = if @isGeneralChat then @experimentContextService.restoreGeneralChatMessages() else []

        @xmppService.on('message_in', @onMessageRecieved)
        @experimentContextService.addSwitchListener @disconnect
        @xmppService.onConnectionResumed(@connect)

        @connect()

    ###
    * Connect to group chat. If xmpp connection is not established yet wait till it
    * is opened and then join the group chat
    ###
    connect: () =>
        if (@xmppService.isConnectedToServer())
            @xmppService.connectToGroupchat(@chatUrl, @$scope.login)
        else
            xs = @xmppService
            cu = @chatUrl
            l = @$scope.login
            @onConnectCallback = () ->
                xs.connectToGroupchat(cu, l)
            @xmppService.on('onconnect', @onConnectCallback)

    ###
     When user is leaving experiment clear the chat state:
        - leave the group chat
        - unregister event listeners
        - for general chat store messages used by users
    ###
    disconnect: () =>
        @xmppService.leaveGroupchat(@chatUrl, @$scope.login)
        @experimentContextService.removeSwitchListener @disconnect
        @xmppService.off('onconnect', @onConnectCallback)
        @xmppService.off('message_in', @onMessageRecieved)
        if @isGeneralChat then @experimentContextService.storeGeneralChatMessages(@$scope.messages)

    send: () =>
        msg = @$scope.myMsg
        if (msg != null and msg != '')
            @xmppService.sendMsg(msg, @chatUrl, true)
        @$scope.myMsg = ''

    onMessageRecieved: (msg) =>
        # present messages only from one chat - generated in constructor
        # present private and general messages from room
        mt = msg.getType()
        if msg.getFrom().indexOf(@chatUrl) != -1 and (mt == "groupchat" or mt = "chat")
            nick = msg.getFrom().substr(msg.getFrom().indexOf('/') + 1)
            # messages from myself have empty nick
            nick = @$scope.login if nick == ""
            @$scope.$apply (s) ->
                # display new message
                s.messages.unshift({user: nick, text: msg.getBody()})
                # remember last N messages
                if (s.messages.length > 50)
                    s.messages = s.messages.slice(0, 50)
