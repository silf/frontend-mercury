'use strict'

class Fake extends Controller

    constructor: ($scope, Experiment) ->

        experiment = new Experiment('fakeroom@muc.pip.ilf.edu.pl')

        chartData = []
        currentDataPoints = []
        x = 0
        seriesNo = 0

        $scope.seriesStarted = false

        experiment.onDisconnect ->
            $scope.fake_connected = no

        $scope.connect = ->
            alias = prompt "Podaj alias"
            $scope.fake_connected = experiment.connect alias
            experiment.onResult( (data) ->
                console.log data
                for i in [0..3]
                    x = x + 1
                    currentDataPoints.push {
                        x: x
                        y: data.value[i]
                    }
                console.log currentDataPoints
                chart.render()
            )
            experiment.onStopSeries( ->
                $scope.$apply(
                    $scope.seriesStarted = false
                )
            )

        $scope.getModes = ->
            experiment.getModes (data) ->
                console.log data
                console.log Object.keys(data)
                $scope.$apply(
                    $scope.modes = data
                    console.log Object.keys($scope.modes)
                    console.log $scope.modes["mode"]
                )

        $scope.setMode = (mode) ->
            console.log mode
            experiment.setMode mode, (data) ->
                console.log data.settings
                $scope.$apply(
                    $scope.settings = data.settings
                )

        chart = new CanvasJS.Chart "chartContainer", {
            zoomEnabled: yes
            title:
                text: 'fake data'
            axisX:
                title: 'nr pomiaru'
            axisY:
                title: 'wartość'
            data: chartData
        }

        $scope.startSeries = ->
            doc = angular.element document.documentElement
            form = doc.find('form').eq(1)[0]
            settings = {}
            for key, value of $scope.settings
                val = form[key].value
                try
                    sval = val
                    val = parseInt val, 10
                    console.log "#{sval} -> #{val}"
                finally
                    settings[key] = { value: val }
            console.log settings
            experiment.startSeries settings, ()->
                $scope.$apply(
                    $scope.seriesStarted = true
                )
                seriesNo += 1
                currentDataPoints = []
                chartData.push(
                    type: 'line'
                    showInLegend: yes
                    name: "seria #{seriesNo}"
                    dataPoints: currentDataPoints
                )
                x = 0

        $scope.stopSeries = ->
            experiment.stopSeries('dupa')