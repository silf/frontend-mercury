'use strict'

###*
* @ngdoc controller
* @id mercury.controller:main
* @name mercury.controller:mainCtrl
* @requires ng.$scope
* @requires ng.$location
* @requires mercury.service:xmpp
* @requires mercury.service:config
* @requires mercury.service:experimentUtil
*
* @property {bool} connected connection (with server) flag
* @property {array} expList list of all available experiments
*
* @description main conroller used for all views on top level
###
class Main extends Controller

    constructor: (@$scope, @$location, @$timeout, @xmppService, @configService, @experimentContextService, @languageService, @gettext, @experimentUtilService) ->
        @connected = @xmppService.connected
        @expList = @configService.experiments.query((experiments) =>
            for exp in experiments
                if $location.path().replace('/experiment/', '') == exp.codename
                    exp.active = yes
                else
                    exp.active = false
                exp.display_name = experimentUtilService.getExperimentDisplayName(exp.display_name, exp.codename)
        )

        @navbarCollapsed = yes

        @xmppService.on('onconnect', =>
            console.debug 'connected'
            @connected = yes
            @connecting = no
            @$scope.$apply()
            console.log 'CONNECTED'
        )

        @xmppService.on('ondisconnect', =>
            @connected = no
            @connecting = no
            console.log 'DISCONNECTED'
        )

        console.log 'CONNECTING'

        @$scope.welcomeMsg = @gettext('Choose experiment from the menu.')
        @configService.credentials.query((data) =>
            @$scope.username = data.username
            @$scope.login = data.login
            @xmppService.connect data.login, data.password
            languageService.setLanguage data.language
        , (errData) =>
            if errData.status = 403
                @$scope.welcomeMsg = @gettext('Please login first to use experiments.')
            else
                @$scope.welcomeMsg = @gettext('There was an error communicating with the server - please reload the page.')
        )

    ###$scope.connect = ->
        Config.credentials.query((data) ->
            console.log data
            $scope.username = data.username
            xmppService.connect data.login, data.password
        )###

    ################## PUBLIC API ##################

    ###*
    * @doc method
    * @methodOf mercury.controller:main
    * @name mainCtrl#disconnect
    * @description disconnects from server
    ###
    disconnect: =>
        console.log 'DISCONNECTING'
        @xmppService.disconnect()

    ###*
    * @doc method
    * @methodOf mercury.controller:main
    * @name mainCtrl#chooseExp
    * @param {string} experiment code name to choose
    * @description switches to experiment url
    ###
    chooseExp: (expName) =>
        @experimentContextService.onExperimentSwitch(expName)
        console.info "switch experiment: " + expName
        @$location.path "/experiment/#{expName}"

    initView: ->
        console.log 'initView'
        #@$timeout ->
            #$('select').select2({dropdownCssClass: 'dropdown-inverse'})
            #$(':checkbox').radiocheck()
            #$(':radio').radiocheck()


