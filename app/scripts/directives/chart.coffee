'use strict'

###*
* @ngdoc directive
* @name mercury.directive:chart
* @requires mercury.factory:utils
* @param {array} data array of two points (x, y) arrays, e.g. [[1,2],[2,4],[3,9]] for y = x^2
* @param {string} title chart title
* @param {string} labelX label for axis x
* @param {string} labelY label for axis y
* @description Panel using CanvasJS library to render chart with live data.
###

class Chart extends Directive

    constructor: (Utils) ->
        init()
        return new ChartInstance(Utils)

    init = ->
        CanvasJS.addCultureInfo("pl",
            {
                decimalSeparator: ","
                digitGroupSeparator: " " # non-breaking space
                zoomText: "powiększ"
                panText: "przesuń"
                resetText: "przywróć"
                days: ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"]
            })



class ChartInstance

    constructor: (@Utils) ->
        @scope =
            data: '='
            title: '='
            labelX: '='
            labelY: '='
            type: '='

    templateUrl: 'scripts/directives/chart.tpl.html'

    restrict: 'E'

    link: (scope, element, attrs) =>
        @id = @Utils.uuid()
        scope.id = @id
        @chart = null

        scope.$watch('data', (value) =>
            @data = value
            @update()
        , yes)
        scope.$watch('title', (value) =>
            @getChart().options.title.text = value
            @update()
        )
        scope.$watch('labelX', (value) =>
            @getChart().options.axisX.title = value
            @update()
        )
        scope.$watch('labelY', (value) =>
            @getChart().options.axisY.title = value
            @update()
        )
        scope.$watch('type', (value) =>
            if @data
                for series in @data
                    series.type = value
            @update()
        )

    getChart: =>
        if not @chart
            @chart = @initChart()
        console.info 'chart is'
        console.info @chart
        @updateSize(@chart)
        return @chart

    updateSize: (chart) =>
        if chart.options
            container = angular.element("##{@id}")
            width = container.width()
            container.find('canvas').attr('width', width)
            if width != chart.options.width
                chart.options.width = width
                if chart.options.data
                    chart.render()

    initChart: =>
        new CanvasJS.Chart(@id, {
            width: 400
            zoomEnabled: yes
            culture: 'pl'
            theme: 'theme2'
            title:
                text: 'Wyniki'
                fontSize: 20
                fontWeight: 'normal'
            axisX:
                title: 'oś X'
                titleFontSize: 18
                labelFontSize: 15
            axisY:
                title: 'oś Y'
                titleFontSize: 18
                labelFontSize: 15
                includeZero: no
            data: []
        })


    update: =>
        if @getChart() and @data and @data.length
            @chart.options.data = @data
            @chart.render()

