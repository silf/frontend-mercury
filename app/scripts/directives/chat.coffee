'use strict'


###*
* @ngdoc directive
* @name mercury.directive:chat
* @requires mercury.controller:xmppChat
* @description Chat markup - use with XmppChat controler
###
class Chat extends Directive

    constructor: ->
        return {
            templateUrl: 'scripts/directives/chat.tpl.html'
            restrict: 'E'
        }