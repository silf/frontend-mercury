'use strict'


###*
* @ngdoc directive
* @name mercury.directive:camera
* @requires ng.$utils
* @description Camera viewport via websockets
###
class Camera extends Directive

    constructor: ->
        return new CameraInstance


class CameraInstance

    constructor: ->
        @scope =
            enabled: '='
            url: '='

    templateUrl: 'scripts/directives/camera.tpl.html'

    restrict: 'E'

    link: (scope, element, attrs) =>
        scope.enabled ?= yes

        scope.data = 'images/camera.svg'

        @reader = new FileReader()
        @reader.onloadend = =>
            scope.$apply( =>
                if @reader.result and @working()
                    scope.data = @reader.result.replace /^.*base64/, 'data:image/jpeg;base64'
            )

        if typeof(WebSocket) == 'function'
            scope.$watch('url', (value) =>
                @url = value
                @run element
            )
        else
            alert 'WebSocket not supported'

        scope.$watch('enabled', (value) =>
            @enabled = value
            @run element
        )

    run: (element) =>
        if @url
            if @enabled
                element.find('img').removeClass('disabled')
                @cam = new WebSocket(@url)
                @cam.onopen = =>
                    if @enabled and @working()
                        @cam.send 'next'
                @cam.onmessage = (msg) =>
                    if msg?.data?
                        @reader.readAsDataURL msg.data
                    if @enabled and @working()
                        @cam.send 'next'
                @cam.onerror = (e) ->
                    console.error e
            else
                console.info 'Camera is disabled'
                element.find('img').addClass('disabled')
        else
            console.warn 'Camera url not defined'

    working: =>
        @url and @cam and @cam.readyState == WebSocket.OPEN
