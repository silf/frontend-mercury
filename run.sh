#!/bin/sh

SCRIPTS_FILE=`ls scripts/*.scripts.js`

cp scripts/scripts.orig.js $SCRIPTS_FILE

if [ ! -z "$WWW_HOST" ]; then
    echo "changing WWW host to $WWW_HOST"
    sed -i'' "s|{WWW_HOST:\"[^\"]*\"}|{WWW_HOST:\"$WWW_HOST\"}|" $SCRIPTS_FILE
fi

