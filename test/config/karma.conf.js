// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function(config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '../..',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      'app/components/jquery/dist/jquery.js',
      'app/components/angular/angular.js',
      'app/components/angular-route/angular-route.js',
      'app/components/angular-sanitize/angular-sanitize.js',
      'app/components/angular-resource/angular-resource.js',
      'app/components/angular-mocks/angular-mocks.js',
	  'app/components/angular-bootstrap/ui-bootstrap.js',
      'app/components/angular-markdown-new/angular.markdown.js',
      'app/components/bootstrap-material-design/scripts/material.js',
      'app/components/angular-gettext/dist/angular-gettext.js',
      'app/scripts/**/*.tpl.html',
      '.tmp/scripts/*.js',
      '.tmp/scripts/**/*.js',
      '.tmp/spec/**/*.js'
    ],

    preprocessors: {
      'app/scripts/**/*.tpl.html': 'ng-html2js'
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'app/',
      moduleName: 'templates'
    },

    // list of files / patterns to exclude
    exclude: [],

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['PhantomJS'],

    // If browser does not capture in given timeout [ms], kill it
    //captureTimeout: 20000,

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true
  });
};
