'use strict'

describe 'Main view', ->

    beforeEach ->
        browser().navigateTo '/index.html'


    it 'should filter the phone list as user types into the search box', ->
        expect(repeater('li').count()).toBe 3