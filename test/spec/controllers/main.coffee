'use strict'

class XMPP
    connected: no
    on: (e, h) =>
        hs = @hs or {}
        a = hs[e] or []
        a.push h
        hs[e] = a
        @hs = hs
    trigger: (e) =>
        hs = @hs[e]
        if hs
            for h in hs
                h()
    connect: =>
        @connected = yes
        @trigger 'onconnect'
    disconnect: =>
        @trigger 'ondisconnect'

describe 'Controller: mainCtrl', ->

    # load the controller's module
    beforeEach module 'mercury'
    # load BootstrapUI module
    beforeEach module 'ui.bootstrap'

    mainCtrl = {}
    scope = {}

    # Initialize the controller and a mock scope
    beforeEach inject ($controller, $rootScope) ->
        scope = $rootScope.$new()
        xmpp = new XMPP
        config = {
            experiments:
                query: (cb) ->
                    result = []
                    if cb
                        cb result
                    result
            credentials:
                query: (cb) ->
                    result = {}
                    if cb
                        cb result
                    result
        }

        mainCtrl = $controller 'mainCtrl', {
            $scope: scope
            xmppService: xmpp
            configService: config
        }

    it 'should be connected on startup', () ->
        expect(mainCtrl.connected).toBe true

    it 'should change connected to false after disconnect', () ->
        mainCtrl.connected = true
        mainCtrl.disconnect()
        expect(mainCtrl.connected).toBe false
