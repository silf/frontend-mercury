'use strict'

describe 'Directive: camera', () ->

  # load the directive's module
  beforeEach module 'mercury'

  beforeEach module 'templates'

  scope = {}
  element = {}

  beforeEach inject ($rootScope, $compile) ->
    scope = $rootScope.$new()
    element = angular.element '<camera url="xxx" enabled="true"></camera>'
    element = $compile(element) scope
    scope.$digest()

  it 'should make image visible', ->
    imgs = element.find('img')
    expect(imgs.length).toBe 1
    expect(imgs.eq(0).attr('id')).toBe 'cam'
