FROM debian:jessie

ENV DIR=/www/mercury

COPY dist $DIR
COPY run.sh $DIR/

WORKDIR $DIR

RUN cp scripts/*.scripts.js scripts/scripts.orig.js

VOLUME ["$DIR"]

CMD ["./run.sh"]
