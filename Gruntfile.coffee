'use strict'

LIVERELOAD_PORT = 35729
lrSnippet = require('connect-livereload') { port: LIVERELOAD_PORT }

ngClassify = require 'ng-classify'

mountFolder = (connect, dir) ->
    connect.static require('path').resolve(dir)


## Globbing
# for performance reasons we're only matching one level down:
# 'test/spec/{,*/}*.js'
# use this if you want to recursively match all subfolders:
# 'test/spec/**/*.js'

module.exports = (grunt) ->
    require('load-grunt-tasks') grunt
    require('time-grunt') grunt

    # configurable paths
    yeomanConfig =
        app: 'app'
        dist: 'dist'
        tmp: '.tmp'
        cfg: 'config'

    try
        yeomanConfig.app = require('./bower.json').appPath || yeomanConfig.app
    catch e
        { }

    grunt.initConfig
        yeoman: yeomanConfig

        watch:
            ngClassify:
                files: ['<%= yeoman.app %>/scripts/{,*/}*.coffee']
                tasks: ['ngClassify']
            coffee:
                files: ['<%= yeoman.tmp %>/scripts/{,*/}*.coffee']
                tasks: ['coffee:dist']
            coffeeUnit:
                files: ['test/spec/{,*/}*.coffee']
                tasks: ['coffee:unit']
            coffeeE2E:
                files: ['test/e2e/{,*/}*.coffee']
                tasks: ['coffee:e2e']
            less:
                files: ['<%= yeoman.app %>/styles/{,*/}*.less']
                tasks: ['less']
            styles:
                files: ['<%= yeoman.app %>/styles/{,*/}*.css']
                tasks: ['copy:styles', 'autoprefixer']
            html2js:
                files: ['<%= yeoman.app %>/scripts/{,*/}*.tpl.html']
                tasks: ['html2js']
            livereload:
                options:
                    livereload: LIVERELOAD_PORT
                files: [
                    '<%= yeoman.app %>/{,*/}*.html'
                    '{<%= yeoman.tmp %>,<%= yeoman.app %>}/styles/{,*/}*.css'
                    '{<%= yeoman.tmp %>,<%= yeoman.app %>}/scripts/{,*/}*.js'
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]

        "regex-replace":
            # patch bootstrap to use font-awesome
            bootsome:
                src: ['<%= yeoman.app %>/components/bootstrap/less/bootstrap.less']
                actions:[{
                    name: 'font-awesome patch for bootstrap'
                    search: '@import "sprites\\.less";'
                    replace: '@import "../../font-awesome/less/font-awesome.less";'
                    flags: 'gm'
                }]
            someboot:
                src: ['<%= yeoman.app %>/components/font-awesome/less/variables.less']
                actions:[{
                    name: 'bootstrap patch for font-awesome'
                    search: '@FontAwesomePath:\\s*"\\.\\.\\/font";'
                    replace: '@FontAwesomePath:    "../components/font-awesome/font";'
                    flags: 'gm'
                }]
            bsMaterial:
                src: ['<%= yeoman.app %>/components/bootstrap-material-design/less/_import-bs-less.less']
                actions:[{
                    name: 'bootstrap-material patch for bs path'
                    search: '@import "\.\./bower_components/bootstrap/'
                    replace: '@import "\.\./\.\./bootstrap/'
                    flags: 'gm'
                }]

        autoprefixer:
            options: ['last 1 version']
            dist:
                files: [{
                    expand: true
                    cwd: '<%= yeoman.tmp %>/styles/'
                    src: '{,*/}*.css'
                    dest: '<%= yeoman.tmp %>/styles/'
                }]

        connect:
            options:
                port: 9000
                # Change this to '0.0.0.0' to access the server from outside.
                hostname: '0.0.0.0'
            livereload:
                options:
                    middleware: (connect) ->
                        return [
                            lrSnippet,
                            mountFolder(connect, yeomanConfig.tmp),
                            mountFolder(connect, yeomanConfig.app)
                        ]
            e2e:
                options:
                    middleware: (connect) ->
                        return [
                            mountFolder(connect, yeomanConfig.tmp),
                            mountFolder(connect, yeomanConfig.app)
                        ]
            dist:
                options:
                    middleware: (connect) ->
                        return [
                            mountFolder(connect, yeomanConfig.dist)
                        ]

        open:
            server:
                url: 'http://localhost:<%= connect.options.port %>'

        clean:
            dist:
                files: [{
                    dot: true
                    src: [
                        yeomanConfig.tmp,
                        '<%= yeoman.dist %>/*',
                        '!<%= yeoman.dist %>/.git*'
                    ]
                }]
            server: yeomanConfig.tmp

        jshint:
            options:
                jshintrc: '.jshintrc'
            all: [
                'Gruntfile.js',
                '<%= yeoman.app %>/scripts/{,*/}*.js'
            ]

        coffee:
            options:
                sourceMap: true
                sourceRoot: ''
            dist:
                files: [{
                    expand: true
                    cwd: '<%= yeoman.tmp %>/scripts'
                    src: '{,*/}*.coffee'
                    dest: '<%= yeoman.tmp %>/scripts'
                    ext: '.js'
                }]
            unit:
                files: [{
                    expand: true
                    cwd: 'test/spec'
                    src: '{,*/}*.coffee'
                    dest: '<%= yeoman.tmp %>/spec'
                    ext: '.js'
                }]
            e2e:
                files: [{
                    expand: true
                    cwd: 'test/e2e'
                    src: '{,*/}*.coffee'
                    dest: '<%= yeoman.tmp %>/e2e'
                    ext: '.js'
                }]

        coffeelint:
            app:
                files:
                    src: ['<%= yeoman.app %>/scripts/{,*/}*.coffee']
                options:
                    indentation:
                        value: 4
                    max_line_length:
                        level: 'ignore'
                    cyclomatic_complexity:
                        level: 'warn'
                    space_operators:
                        level: 'warn'

        less:
            dist:
                options:
                    compile: true
                files:
                    '<%= yeoman.tmp %>/styles/main.css': ['<%= yeoman.app %>/styles/main.less']

        # not used since Uglify task does concat,
        # but still available if needed
        #concat: {
        #        dist: {
        #            files: {
        #                '<%= yeoman.dist %>/scripts/scripts.js': [
        #                    '<%= yeoman.tmp %>/scripts/{,*/}*.js',
        #                    '<%= yeoman.app %>/scripts/{,*/}*.js'
        #                ]
        #            }
        #        }
        #},

        rev:
            dist:
                files:
                    src: [
                        '<%= yeoman.dist %>/scripts/{,*/}*.js',
                        '<%= yeoman.dist %>/styles/{,*/}*.css',
                        '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                        '<%= yeoman.dist %>/styles/fonts/*'
                    ]

        useminPrepare:
            html: '<%= yeoman.app %>/index.html'
            options:
                dest: '<%= yeoman.dist %>'

        usemin:
            html: ['<%= yeoman.dist %>/{,*/}*.html']
            css: ['<%= yeoman.dist %>/styles/{,*/}*.css']
            js: ['<%= yeoman.dist %>/scripts/*.js']
            options:
                assetsDirs: ['<%= yeoman.dist %>', '<%= yeoman.dist %>/images']
                patterns:
                    js: [
                        [/(images\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the JS to reference our revved images']
                    ]

        imagemin:
            dist:
                files: [{
                    expand: true
                    cwd: '<%= yeoman.app %>/images'
                    src: '{,*/}*.{png,jpg,jpeg}'
                    dest: '<%= yeoman.dist %>/images'
                }]

        svgmin:
            dist:
                files: [{
                    expand: true
                    cwd: '<%= yeoman.app %>/images'
                    src: '{,*/}*.svg'
                    dest: '<%= yeoman.dist %>/images'
                }]

        cssmin: {
            # By default, your `index.html` <!-- Usemin Block --> will take care of
            # minification. This option is pre-configured if you do not wish to use
            # Usemin blocks.
            # dist: {
            #   files: {
            #     '<%= yeoman.dist %>/styles/main.css': [
            #       '<%= yeoman.tmp %>/styles/{,*/}*.css',
            #       '<%= yeoman.app %>/styles/{,*/}*.css'
            #     ]
            #   }
            # }
        }

        htmlmin:
            dist:
                options: {
                    ###removeCommentsFromCDATA: true,
                    // https://github.com/yeoman/grunt-usemin/issues/44
                    //collapseWhitespace: true,
                    collapseBooleanAttributes: true,
                    removeAttributeQuotes: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true,
                    removeEmptyAttributes: true,
                    removeOptionalTags: true ###
                }
                files: [{
                    expand: true
                    cwd: '<%= yeoman.app %>'
                    src: ['*.html', 'views/*.html']
                    dest: '<%= yeoman.dist %>'
                }]

        # Put files not handled in other tasks here
        copy:
            dist:
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>',
                    src: [
                        '*.{ico,png,txt}',
                        '.htaccess',
                        'components/**/*',
                        'libs/**/*',
                        'images/{,*/}*.{gif,webp}',
                        'styles/fonts/*',
                        'manifest.appcache'
                    ]}, {
                    expand: true,
                    cwd: '<%= yeoman.tmp %>/images',
                    dest: '<%= yeoman.dist %>/images',
                    src: [
                        'generated/*'
                    ]}, {
                    expand: true,
                    cwd: '<%= yeoman.tmp %>/fonts',
                    dest: '<%= yeoman.dist %>/fonts',
                    src: [
                        '*'
                    ]}
                ]
            fonts:
                expand: true
                cwd: '<%= yeoman.app %>'
                dest: '<%= yeoman.tmp %>/fonts/'
                src: 'components/**/fonts/*'
                flatten: true
            styles:
                expand: true
                cwd: '<%= yeoman.app %>/styles'
                dest: '<%= yeoman.tmp %>/styles/'
                src: '{,*/}*.css'
            remoteXmppServerConfig:
                expand: false
                src: '<%= yeoman.cfg %>/remoteServerConfig.coffee'
                dest: '<%= yeoman.app %>/scripts/services/config.coffee'
            localXmppServerConfig:
                expand: false
                src: '<%= yeoman.cfg %>/localServerConfig.coffee'
                dest: '<%= yeoman.app %>/scripts/services/config.coffee'

        concurrent:
            server: [
                'coffee:dist',
                'html2js',
                'less',
                'copy:styles',
                'copy:fonts',
                'nggettext_compile'
            ]
            unit: [
                'coffee:dist',
                'html2js',
                'coffee:unit',
                'copy:styles'
            ]
            e2e: [
                'coffee:dist',
                'html2js',
                'coffee:e2e',
                'less',
                'copy:styles'
            ]
            test: [
                'coffee:dist',
                'html2js',
                'coffee:unit',
                'coffee:e2e',
                'less',
                'copy:styles'
            ]
            dist: [
                'coffee:dist',
                'html2js',
                'less',
                'copy:styles',
                'copy:fonts',
                'imagemin',
                'svgmin',
                'htmlmin',
                'nggettext_compile'
            ]

        karma:
            unit:
                configFile: 'test/config/karma.conf.js'
                singleRun: true
            e2e:
                configFile: 'test/config/karma-e2e.conf.js'
                singleRun: true

        cdnify:
            dist:
                html: ['<%= yeoman.dist %>/*.html']

        ngAnnotate:
            dist:
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>/scripts',
                    src: '*.js',
                    dest: '<%= yeoman.dist %>/scripts'
                }]

        ngClassify:
            app:
                files: [
                    cwd: '<%= yeoman.app %>/scripts'
                    src: '**/*.coffee'
                    dest: '<%= yeoman.tmp %>/scripts'
                    expand: true
                ]
                options:
                    appName: 'mercury'
                    controller:
                        suffix: 'Ctrl'

        uglify:
            dist:
                files:
                    '<%= yeoman.dist %>/scripts/scripts.js': [
                        '<%= yeoman.dist %>/scripts/scripts.js'
                    ]
            # options:
            #    sourceMap: true
            #    beautify: true
            #    mangle: false
            #    sourceMapIncludeSources: true
            #    compress: false

        rsync:
            options:
                args: ["--verbose", "--archive"]
                exclude: [".git*", "node_modules", ".htaccess"]
                recursive: true
            prod:
                options:
                    src: 'dist/'
                    dest: '/www/mercury'
                    host: 'www@ilf.fizyka.pw.edu.pl'
            dev:
                options:
                    src: 'dist/'
                    dest: '/www/mercury'
                    host: 'www@dilf.fizyka.pw.edu.pl'

        ngconstant:
            dev:
                options:
                    name: 'Constants'
                    dest: '<%= yeoman.tmp %>/scripts/config/constants.js'
                constants:
                    Constants: grunt.file.readJSON 'config/dev.json'
            prod:
                options:
                    name: 'Constants'
                    dest: '<%= yeoman.tmp %>/scripts/config/constants.js'
                constants:
                    Constants: grunt.file.readJSON 'config/prod.json'

        html2js:
            options:
                base: '<%= yeoman.app %>'
                module: 'mercury-templates'
            main:
                src: '<%= yeoman.app %>/scripts/**/*.tpl.html'
                dest: '<%= yeoman.tmp %>/scripts/templates.js'

        nggettext_extract:
            pot:
                files:
                    'lang/template.pot': [
                        '<%= yeoman.app %>/*.html',
                        '<%= yeoman.app %>/views/{,*/}*.html',
                        '<%= yeoman.app %>/scripts/{,*/}*.html',
                        '<%= yeoman.tmp %>/scripts/{,*/}*.js'
                    ]
        nggettext_compile:
            all:
                files:
                    '<%= yeoman.tmp %>/scripts/translations.js': ['lang/*.po']

        ngdocs:
            options:
                dest: '.tmp/docs'
                startPage: '/overview/intro'
                bestMatch: yes
                inlinePartials: no
            overview:
                src: ['docs/*.ngdoc']
                title: 'Overview'
            api:
                src: ['.tmp/scripts/**/*.js']

        docular:
            docular_webapp_target: '.tmp/docs'
            groups: [
                {
                    groupTitle: 'Mercury',
                    groupId: 'mercury',
                    groupIcon: 'icon-beaker',
                    sections: [
                        {
                            id: "overview",
                            title: "Overview",
                            docs: [
                                'docs/'
                            ]
                        },
                        {
                            id: "api",
                            title: "API",
                            scripts: [
                                '.tmp/scripts/'
                            ]
                        }
                    ]
                }
            ]

        bump:
            options:
                files: ['package.json', 'bower.json']

        dock:
            options:
                images:
                    mercury:
                        dockerfile: 'Dockerfile'
                        options:
                            build:
                                t: 'ilfpw/frontend-mercury'


    grunt.registerTask 'server', (target, open) ->
        if target == 'dist'
            return grunt.task.run(['build:dev', 'open', 'connect:dist:keepalive'])

        copyConfig = if target == 'remote' then 'copy:remoteXmppServerConfig' else 'copy:localXmppServerConfig'
        open = if open == 'false' or open == 'no' then copyConfig else 'open'

        grunt.task.run [
            'clean:server',
            'ngconstant:dev',
            copyConfig,
            'coffeelint',
            'ngClassify',
            'regex-replace:bsMaterial',
            'concurrent:server',
            #'autoprefixer',
            'connect:livereload',
            open,
            'watch'
        ]

    grunt.registerTask 'unit', [
        'clean:server',
        'ngconstant:dev',
        'ngClassify',
        'concurrent:unit',
        'autoprefixer',
        'karma:unit'
    ]

    grunt.registerTask 'e2e', [
        'clean:server',
        'ngconstant:dev',
        'ngClassify',
        'concurrent:e2e',
        'autoprefixer',
        'connect:e2e',
        'karma:e2e'
    ]

    grunt.registerTask 'test', [
        'clean:server',
        'ngconstant:dev',
        'ngClassify',
        'concurrent:test',
        'autoprefixer',
        'connect:e2e',
        'karma'
    ]

    grunt.registerTask 'build', (target) ->

        if not target
            target = 'prod'

        copyConfig = if target == 'prod' then 'copy:remoteXmppServerConfig' else 'copy:localXmppServerConfig'

        grunt.task.run [
            'clean:dist',
            "ngconstant:#{target}",
            copyConfig,
            'coffeelint',
            'ngClassify',
            'useminPrepare',
            'regex-replace:bsMaterial',
            'concurrent:dist',
            'autoprefixer',
            'concat',
            'copy:dist',
            'cdnify',
            'ngAnnotate',
            'imagemin',
            'svgmin',
            'htmlmin',
            'cssmin',
            'uglify',
            'rev',
            'usemin'
        ]

    grunt.registerTask 'doc', [
        'clean:server',
        'ngClassify',
        'coffee:dist',
        'ngdocs'
    ]

    grunt.registerTask 'default', [
        'jshint',
        'test',
        'build'
    ]

    grunt.registerTask 'extract', [
        'ngClassify',
        'coffee:dist',
        'nggettext_extract'
    ]
