#!/usr/bin/env bash

SCRIPT_HOME="$( cd "$( dirname "$0" )" && pwd )"
IMAGE="mercury-dev"

if [ `docker images $IMAGE | wc -l` = 1 ]; then
    docker build -t $IMAGE dev
fi

CMD="docker run --rm -it -v $SCRIPT_HOME:/data -p 9000:9000 $IMAGE"

$CMD "$@"
