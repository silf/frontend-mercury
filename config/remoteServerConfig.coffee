'use strict'

###*
* @ngdoc service
* @id mercury.service:config
* @name configService
* @requires ng.$resource
* @requires mercury.factory:constants
* @description system configuration
###
class Config extends Service

    constructor: ($resource, Constants) ->
        baseUrl = 'silf/api'

        ###*
        * @doc property
        * @propertyOf mercury.service:config
        * @name configService#xmpp_server
        * @description xmpp server info
        ###
        @xmpp_server = $resource("#{Constants.WWW_HOST}#{baseUrl}/server", { },
                            query:
                                method: 'GET'
                                isArray: no
                                cache: yes
                                withCredentials: yes
                                interceptor:
                                    responseError: (rejection) ->
                                        console.log 'responseError'
                                        console.log rejection
                        )

        ###*
        * @doc property
        * @propertyOf mercury.service:config
        * @name configService#experiments
        * @description experiments info
        ###
        @experiments = $resource("#{Constants.WWW_HOST}#{baseUrl}/experiments", { },
                            query:
                                method: 'GET'
                                isArray: yes
                                cache: yes
                                interceptor:
                                    responseError: (rejection) ->
                                        console.log 'responseError'
                                        console.log rejection
                        )

        ###*
        * @doc property
        * @propertyOf mercury.service:config
        * @name configService#credentials
        * @description user credentials
        ###
        @credentials = $resource("#{Constants.WWW_HOST}#{baseUrl}/my_credentials", { },
                            query:
                                method: 'GET'
                                isArray: no
                                cache: yes
                                withCredentials: yes
                                interceptor:
                                    response: (response) ->
                                        login = (response.resource.jid.match /[^@]*/)[0]
                                        username = (login.match /user\$(.*)/)
                                        username = if username then username[1] else ''
                                        response.resource.login = login
                                        response.resource.username = username
                                        response.resource
                                    responseError: (rejection) ->
                                        console.log 'responseError'
                                        console.log rejection
                        )

        @generalRoomName = "general-chat"

        # this is list of supported versions, each version must be present explicitly in list. No version range is allowed!
        @supportedProtocolVersions = ["1.1.0", "1.0.0"] # first supported version is chosen

        # timeout used between sending ping packets on idle connection
        @pingTimeout = 5000

        # url template for download of series data based on uuid of the series
        @expResultsUrl = "#{Constants.WWW_HOST}silf/experiment_results/uuid/DATA_SERIES_ID/?format=csv"

        # timeout for negotiating protocol version
        @protocolVersionTimeout = 2800
