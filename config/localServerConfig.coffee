'use strict'

###*
* @ngdoc service
* @id mercury.service:config
* @name configService
* @description system configuration
###
class Config extends Service

    constructor: ->

    ###*
    * @doc property
    * @propertyOf mercury.service:config
    * @name configService#xmpp_server
    * @description xmpp server info
    ###
    xmpp_server: {
        query: (callback) ->
            servData = {
                address: 'ws://127.0.0.1:5290', # 'ws://10.66.0.103:5290',
                domain: 'silf.dev.pl', # 'dilf.fizyka.pw.edu.pl',
                language: 'pl'
            }
            callback(servData)
    }

    ###*
    * @doc property
    * @propertyOf mercury.service:config
    * @name configService#experiments
    * @description experiments info
    ###
    experiments: {
        query: (callback) ->
            console.log 'in experiments.query'
            console.log callback
            expData = [
                {
                    "url": "test@muc.silf.dev.pl",
                    "codename": "test",
                    "display_name": "Testowy",
                    "max_reservation_length": "2:00:00",
                    "camera_url": null,
                    "docs_url": null
                },
                {
                    "url": "geiger-test@muc.dilf.fizyka.pw.edu.pl",
                    "codename": "geiger-test",
                    "display_name": '{"pl": "Test geigera", "en": "Geiger test (EN)"}',
                    "max_reservation_length": "2:00:00",
                    "camera_url": null,
                    "docs_url": "http://ilf.fizyka.pw.edu.pl/instrukcje/geiger"
                },
                {
                    "url": "attenuation-test@muc.dilf.fizyka.pw.edu.pl",
                    "codename": "attenuation-test",
                    "display_name": "Test osłabienia",
                    "max_reservation_length": "2:00:00",
                    "camera_url": "ws://vilf.fizyka.pw.edu.pl/absorb",
                    "docs_url": null
                },
                {
                    "url": "diody-test@muc.dilf.fizyka.pw.edu.pl",
                    "codename": "diody-test",
                    "display_name": "Test diód",
                    "max_reservation_length": "2:00:00",
                    "camera_url": "ws://vilf.fizyka.pw.edu.pl/bb",
                    "docs_url": null
                },
                {
                    "url": "blackbody-test@muc.dilf.fizyka.pw.edu.pl",
                    "codename": "blackbody-test",
                    "display_name": "Test BB",
                    "max_reservation_length": "2:00:00",
                    "camera_url": "ws://vilf.fizyka.pw.edu.pl/bb",
                    "docs_url": null
                },
                {
                    "url": "snellius-test@muc.dilf.fizyka.pw.edu.pl",
                    "codename": "snellius-test",
                    "display_name": "Test prawa snelliusa",
                    "max_reservation_length": "2:00:00",
                    "camera_url": "ws://vilf.fizyka.pw.edu.pl/bb",
                    "docs_url": null
                },
            ]

            if typeof callback is "function"
                callback(expData)

            return expData
    }

    ###*
    * @doc property
    * @propertyOf mercury.service:config
    * @name configService#credentials
    * @description user credentials
    ###
    credentials: {
        query: (callback) ->
            console.log 'in credentials.query'
            console.log callback
            usrData = {
                login: 'admin',   # uzywane do logowania do serweera XMPP
                username: 'admin',    # wyswietlane na stronie
                password: 'foobarbaz' #'HuEBwmPRYXrtLxwLbYMmpaUnPcoagklkgGFJuHnfoqjPkiKcaEAzyjTkLFZgVYhpgKyrDgVHTGeaXfzQllKwseFHQkKkaRWETJHDXDYBdMubTdSyAGLavrwYEVVYAAdu',
            }
            if callback
                callback(usrData)
            usrData
    }

    generalRoomName: "general-chat"

    # this is list of supported versions, each version must be present explicitly in list. No version range is allowed!
    supportedProtocolVersions: ["1.1.0", "1.0.0"] # first supported version is chosen

    # timeout used between sending ping packets on idle connection
    pingTimeout: 5000

    # url template for download of series data based on uuid of the series
    expResultsUrl: "http://localhost:8000/silf/experiment_results/uuid/DATA_SERIES_ID/?format=csv"

    protocolVersionTimeout: 999999 # for manual testing
