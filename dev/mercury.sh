#!/usr/bin/env bash

case "$1" in
    init)
        npm install
        bower --allow-root install
        git submodule init
        git submodule update
        cd app/libs/jsjac
        make
        ;;
    start)
        grunt server:local:no
        ;;
    build)
        grunt build
        ;;
    *)
        $@
        ;;
esac
